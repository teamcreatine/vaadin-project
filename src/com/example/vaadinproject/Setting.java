package com.example.vaadinproject;

import com.example.vaadinproject.util.StringUtils;

/**
 * Object representing a setting stored in settings.properties.
 * 
 * @author Jukka Viljala <jupevi@utu.fi>
 * 
 */
public class Setting {

    private final String name;
    private final String property;
    private final String defaultValue;

    public Setting(String name, String defaultValue) {
        this(name, StringUtils.toCamelCase(name), defaultValue);
    }

    public Setting(String name, String property, String defaultValue) {
        this.name = name;
        this.property = property;
        this.defaultValue = defaultValue;
    }

    public String getName() {
        return name;
    }

    public String getProperty() {
        return property;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public String getValue() {
        return Settings.get(property);
    }

    public void setValue(String value) {
        Settings.set(property, value);
    }

}
