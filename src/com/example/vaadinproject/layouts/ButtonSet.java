package com.example.vaadinproject.layouts;

import java.util.Iterator;

import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;

/**
 * Groups a set of buttons in a horizontal layout.
 * 
 * @author Jukka Viljala <jupevi@utu.fi>
 * 
 */
public class ButtonSet extends HorizontalLayout {

    /**
     * Creates a new button set containing a variable amount of buttons. Floats
     * layout to the right.
     * 
     * @param buttons
     *            buttons to add
     */
    public ButtonSet(Button... buttons) {
        this("float-right", buttons);
    }

    /**
     * Creates a new button set containing a variable amount of buttons.
     * 
     * @param styleName
     *            style name to apply to container
     * @param buttons
     *            buttons to add
     */
    public ButtonSet(String styleName, Button... buttons) {
        setSpacing(true);
        addComponents(buttons);
        addStyleName(styleName);
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        Iterator<Component> iter = iterator();
        while (iter.hasNext()) {
            iter.next().setEnabled(enabled);
        }
    }

    /**
     * Adds a new button to the end of the layout and applies a style to it.
     * 
     * @param button
     *            button to add
     * @param styleName
     *            style to apply
     */
    public void addComponent(Button button, String styleName) {
        button.addStyleName(styleName);
        addComponent(button);
    }

}
