package com.example.vaadinproject.layouts;

import com.vaadin.ui.Component;
import com.vaadin.ui.VerticalLayout;

/**
 * Vertical layout that automatically adds spacing and optionally margins.
 * 
 * @author Jukka Viljala <jupevi@utu.fi>
 * 
 */
public class DefaultVerticalLayout extends VerticalLayout {

    /**
     * Creates a new layout with spacing and margins.
     */
    public DefaultVerticalLayout() {
        this(true);
    }

    /**
     * Creates a new layout with spacing and margin, containing specified
     * components.
     * 
     * @param components
     *            components to add
     */
    public DefaultVerticalLayout(Component... components) {
        this(true, components);
    }

    /**
     * Creates a new layout with spacing and optionally margins.
     * 
     * @param margins
     *            true to add margins
     */
    public DefaultVerticalLayout(boolean margins) {
        this(margins, new Component[0]);
    }

    /**
     * Creates a new layout with spacing and optionally margins, containing
     * specified components.
     * 
     * @param margins
     *            true to add margins
     */
    public DefaultVerticalLayout(boolean margins, Component... components) {
        super(components);
        setSpacing(true);
        setMargin(margins);
    }

}
