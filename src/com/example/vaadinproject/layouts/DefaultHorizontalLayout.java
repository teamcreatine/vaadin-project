package com.example.vaadinproject.layouts;

import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;

/**
 * Horizontal layout that automatically adds spacing and optionally margins.
 * 
 * @author Jukka Viljala <jupevi@utu.fi>
 * 
 */
public class DefaultHorizontalLayout extends HorizontalLayout {

    /**
     * Creates a new layout with spacing and margins.
     */
    public DefaultHorizontalLayout() {
        this(true);
    }

    /**
     * Creates a new layout with spacing and margin, containing specified
     * components.
     * 
     * @param components
     *            components to add
     */
    public DefaultHorizontalLayout(Component... components) {
        this(true, components);
    }

    /**
     * Creates a new layout with spacing and optionally margins.
     * 
     * @param margins
     *            true to add margins
     */
    public DefaultHorizontalLayout(boolean margins) {
        this(margins, new Component[0]);
    }

    /**
     * Creates a new layout with spacing and optionally margins, containing
     * specified components.
     * 
     * @param margins
     *            true to add margins
     */
    public DefaultHorizontalLayout(boolean margins, Component... components) {
        super(components);
        setSpacing(true);
        setMargin(margins);
    }

}
