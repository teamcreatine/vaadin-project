package com.example.vaadinproject.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import com.example.vaadinproject.Settings;
import com.vaadin.addon.jpacontainer.EntityItem;
import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.addon.jpacontainer.JPAContainerFactory;

/**
 * Static class for creating and caching JPA containers.
 * 
 * @author Jukka Viljala <jupevi@utu.fi>
 * 
 */
public class JPAHelper {

    /**
     * Type of container.
     */
    public enum ContainerType {
        DEFAULT, BATCHABLE
    }

    private static EntityManager em;

    /**
     * Container cache.
     */
    private static Map<ContainerType, Map<Class<?>, JPAContainer<?>>> containers = new HashMap<ContainerType, Map<Class<?>, JPAContainer<?>>>();

    /**
     * Gets the entity manager used in the application. Acts as a singleton.
     * 
     * @return entity manager
     */
    public static EntityManager getManager() {
        if (em == null) {
            // Get new entity manager with stored settings
            Map<String, String> props = new HashMap<String, String>();
            props.put(
                    "javax.persistence.jdbc.url",
                    String.format("jdbc:h2:%s",
                            Settings.getFullPath(Settings.get("databaseUrl"))));
            props.put("javax.persistence.jdbc.user",
                    Settings.get("databaseUser"));
            props.put("javax.persistence.jdbc.password",
                    Settings.get("databasePass"));
            props.put("eclipselink.logging.level", Settings.get("loggingLevel"));
            EntityManagerFactory emf = Persistence.createEntityManagerFactory(
                    "main", props);
            em = emf.createEntityManager();
        }
        return em;
    }

    /**
     * Gets a normal JPA container for entity.
     * 
     * @param type
     *            entity type
     * @return container
     */
    public static <T> JPAContainer<T> getContainer(Class<T> type) {
        return getContainer(type, ContainerType.DEFAULT);
    }

    @SuppressWarnings("unchecked")
    /**
     * Gets a JPA container for entity, specifying container type.
     * 
     * @param type entity type
     * @param containerType container type
     * @return container
     */
    public static <T> JPAContainer<T> getContainer(Class<T> type,
            ContainerType containerType) {
        if (!containers.containsKey(containerType)) {
            containers.put(containerType,
                    new WeakHashMap<Class<?>, JPAContainer<?>>());
        }
        Map<Class<?>, JPAContainer<?>> map = containers.get(containerType);
        if (!map.containsKey(type)) {
            map.put(type, getNewContainer(type, containerType));
        }
        return (JPAContainer<T>) map.get(type);
    }

    /**
     * Gets a new instance of a normal JPA container for entity.
     * 
     * @param type
     *            entity type
     * @return new container
     */
    public static <T> JPAContainer<T> getNewContainer(Class<T> type) {
        return getNewContainer(type, ContainerType.DEFAULT);
    }

    /**
     * Gets a new instance of a JPA container for entity, specifying container
     * type.
     * 
     * @param type
     *            entity type
     * @param containerType
     *            container type
     * @return container
     */
    public static <T> JPAContainer<T> getNewContainer(Class<T> type,
            ContainerType containerType) {
        if (containerType == ContainerType.BATCHABLE) {
            return JPAContainerFactory.makeBatchable(type, getManager());
        }
        return JPAContainerFactory.make(type, getManager());
    }

    /**
     * Gets an entity instance of a type by id from a container, wrapped in an
     * EntityItem.
     * 
     * @param type
     *            entity type to get
     * @param itemId
     *            entity id to get
     * @return
     */
    public static <T> EntityItem<?> getItem(Class<T> type, Object itemId) {
        return getContainer(type).getItem(itemId);
    }

    @SuppressWarnings("unchecked")
    /**
     * Gets an entity instance of a type.
     * 
     * @param type entity type to get
     * @param itemId entity id to get
     * @return
     */
    public static <T> T getEntity(Class<T> type, Object itemId) {
        try {
            return (T) getItem(type, itemId).getEntity();
        } catch (Exception e) {

        }
        return null;
    }

    /**
     * Gets the first item id in an entity container.
     * 
     * @param type
     *            entity type
     * @return first id in container
     */
    public static <T> Object getFirstItemId(Class<T> type) {
        try {
            return getContainer(type).firstItemId();
        } catch (Exception e) {

        }
        return null;
    }

    /**
     * Gets the first entity in an entity container.
     * 
     * @param type
     *            entity type
     * @return first entity in container
     */
    public static <T> T getFirstEntity(Class<T> type) {
        try {
            return getEntity(type, getFirstItemId(type));
        } catch (Exception e) {

        }
        return null;
    }

    /**
     * Manually query an entity, filtering by some column value. Only returns
     * the first entity.
     * 
     * @param type
     *            entity type
     * @param column
     *            filter column
     * @param value
     *            column value
     * @return first entity returned by query
     */
    public static <T> T query(Class<T> type, String column, Object value) {
        String typeName = type.getSimpleName();
        TypedQuery<T> query = em.createQuery(String
                .format("SELECT q FROM %s q WHERE q.%s = '%s'", typeName,
                        column, value), type);
        List<T> results = query.getResultList();
        if (results.size() > 0) {
            return results.get(0);
        }
        return null;
    }

}
