package com.example.vaadinproject.util;

/**
 * String utils class.
 * 
 * @author Jukka Viljala <jupevi@utu.fi>
 * 
 */
public class StringUtils {

    /**
     * Gets camelCased presentation of a string.
     * 
     * @param str
     *            string to convert
     * @return camelCased string
     */
    public static String toCamelCase(String str) {
        String[] parts = str.split(" ");
        String camelCaseString = parts[0].toLowerCase();
        for (int i = 1; i < parts.length; i++) {
            camelCaseString = camelCaseString + toCapitalized(parts[i]);
        }
        return camelCaseString;
    }

    /**
     * Gets Title Cased presentation of a string.
     * 
     * @param str
     *            string to convert
     * @return Title Cased string
     */
    public static String toTitleCase(String str) {
        str = str.replaceAll(String.format("%s|%s|%s",
                "(?<=[A-Z])(?=[A-Z][a-z])", "(?<=[^A-Z])(?=[A-Z])",
                "(?<=[A-Za-z])(?=[^A-Za-z])"), " ");
        return str.substring(0, 1).toUpperCase() + str.substring(1);
    }

    /**
     * Gets the Capitalized presentation of a string.
     * 
     * @param str
     *            string to convert
     * @return Capitalized string
     */
    public static String toCapitalized(String str) {
        return str.substring(0, 1).toUpperCase()
                + str.substring(1).toLowerCase();
    }

    public static String toUncapitalized(String str) {
        return str.substring(0, 1).toLowerCase() + str.substring(1);
    }

    /**
     * Naturally joins an array of strings together. The output looks like this:
     * "string, second string or third string".
     * 
     * Doesn't allow for other glues.
     * 
     * @param array
     *            array of strings to join
     * @return joined string
     */
    public static String join(Object[] array) {
        if (array == null) {
            return null;
        }
        int bufSize = array.length;

        bufSize *= ((array[0] == null ? 16 : array[0].toString().length()) + 1);
        StringBuilder builder = new StringBuilder(bufSize);

        for (int i = 0; i < array.length; i++) {
            if (i == array.length - 1) {
                builder.append(" or ");
            } else if (i > 0) {
                builder.append(", ");
            }
            if (array[i] != null) {
                builder.append(toTitleCase(array[i].toString()));
            }
        }
        return builder.toString();
    }

}
