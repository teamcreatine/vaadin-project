package com.example.vaadinproject.pages;

import com.example.vaadinproject.entities.Currency;
import com.example.vaadinproject.forms.SimpleForm;
import com.example.vaadinproject.forms.fields.FormList;
import com.example.vaadinproject.forms.fields.SimpleList;
import com.example.vaadinproject.layouts.DefaultHorizontalLayout;
import com.vaadin.ui.CustomComponent;

/**
 * Component for listing and editing currencies.
 * 
 * @author Jukka Viljala <jupevi@utu.fi>
 * 
 */
public class CurrenciesPage extends CustomComponent {

    SimpleList<Currency> currencyList;
    SimpleForm<Currency> form;

    public CurrenciesPage() {
        form = new SimpleForm<Currency>(Currency.class);
        currencyList = new FormList<Currency>(Currency.class, new Object[] {
                "id", "name" }, form);
        DefaultHorizontalLayout layout = new DefaultHorizontalLayout();
        layout.addComponents(currencyList, form);
        setCompositionRoot(layout);
    }
}
