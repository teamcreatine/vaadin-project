package com.example.vaadinproject.pages;

import com.example.vaadinproject.entities.Supplier;
import com.example.vaadinproject.forms.SimpleForm;
import com.example.vaadinproject.forms.fields.FormList;
import com.example.vaadinproject.forms.fields.SimpleList;
import com.example.vaadinproject.layouts.DefaultHorizontalLayout;
import com.vaadin.ui.CustomComponent;

/**
 * Component for listing and editing suppliers.
 * 
 * @author Jukka Viljala <jupevi@utu.fi>
 * 
 */
public class SuppliersPage extends CustomComponent {

    SimpleList<Supplier> supplierList;
    SimpleForm<Supplier> form;

    public SuppliersPage() {
        form = new SimpleForm<Supplier>(Supplier.class);
        supplierList = new FormList<Supplier>(Supplier.class, new Object[] {
                "id", "name" }, form);
        DefaultHorizontalLayout layout = new DefaultHorizontalLayout();
        layout.addComponents(supplierList, form);
        setCompositionRoot(layout);
    }

}
