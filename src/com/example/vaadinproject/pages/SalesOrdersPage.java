package com.example.vaadinproject.pages;

import com.example.vaadinproject.entities.SalesOrder;
import com.example.vaadinproject.forms.SalesOrderEditForm;
import com.example.vaadinproject.forms.fields.FormList;
import com.example.vaadinproject.forms.fields.SimpleList;
import com.example.vaadinproject.layouts.DefaultHorizontalLayout;
import com.vaadin.ui.CustomComponent;

/**
 * Component for listing and editing sales orders.
 * 
 * @author Jukka Viljala <jupevi@utu.fi>
 * 
 */
public class SalesOrdersPage extends CustomComponent {

    SimpleList<SalesOrder> orderList;
    SalesOrderEditForm form;

    public SalesOrdersPage() {
        form = new SalesOrderEditForm();
        orderList = new FormList<SalesOrder>(SalesOrder.class, new Object[] {
                "id", "shippingName" }, form);
        DefaultHorizontalLayout layout = new DefaultHorizontalLayout();
        layout.addComponents(orderList, form);
        setCompositionRoot(layout);
    }

    public SalesOrderEditForm getForm() {
        return form;
    }

}
