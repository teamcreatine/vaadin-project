package com.example.vaadinproject.pages;

import com.example.vaadinproject.entities.PaymentTerm;
import com.example.vaadinproject.forms.SimpleForm;
import com.example.vaadinproject.forms.fields.FormList;
import com.example.vaadinproject.forms.fields.SimpleList;
import com.example.vaadinproject.layouts.DefaultHorizontalLayout;
import com.vaadin.ui.CustomComponent;

/**
 * Component for listing and editing payment terms.
 * 
 * @author Jukka Viljala <jupevi@utu.fi>
 * 
 */
public class PaymentTermsPage extends CustomComponent {

    SimpleList<PaymentTerm> paymentTermList;
    SimpleForm<PaymentTerm> form;

    public PaymentTermsPage() {
        form = new SimpleForm<PaymentTerm>(PaymentTerm.class);
        paymentTermList = new FormList<PaymentTerm>(PaymentTerm.class,
                new Object[] { "id", "name" }, form);
        DefaultHorizontalLayout layout = new DefaultHorizontalLayout();
        layout.addComponents(paymentTermList, form);
        setCompositionRoot(layout);
    }
}
