package com.example.vaadinproject.pages;

import com.example.vaadinproject.entities.Invoice;
import com.example.vaadinproject.forms.InvoiceEditForm;
import com.example.vaadinproject.forms.fields.FormList;
import com.example.vaadinproject.forms.fields.SimpleList;
import com.example.vaadinproject.layouts.DefaultHorizontalLayout;
import com.vaadin.ui.CustomComponent;

/**
 * Component for listing and editing invoices.
 * 
 * @author Jukka Viljala <jupevi@utu.fi>
 * 
 */
public class InvoicesPage extends CustomComponent {

    SimpleList<Invoice> invoiceList;
    InvoiceEditForm form;

    public InvoicesPage() {
        form = new InvoiceEditForm();
        invoiceList = new FormList<Invoice>(Invoice.class, new Object[] { "id",
                "name" }, form);
        DefaultHorizontalLayout layout = new DefaultHorizontalLayout();
        layout.addComponents(invoiceList, form);
        setCompositionRoot(layout);
    }

    public InvoiceEditForm getForm() {
        return form;
    }

}
