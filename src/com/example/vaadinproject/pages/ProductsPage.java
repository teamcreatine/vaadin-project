package com.example.vaadinproject.pages;

import com.example.vaadinproject.entities.Product;
import com.example.vaadinproject.forms.ProductEditForm;
import com.example.vaadinproject.forms.fields.FormList;
import com.example.vaadinproject.forms.fields.SimpleList;
import com.example.vaadinproject.layouts.DefaultHorizontalLayout;
import com.vaadin.ui.CustomComponent;

/**
 * Component for listing and editing products.
 * 
 * @author Jukka Viljala <jupevi@utu.fi>
 * 
 */
public class ProductsPage extends CustomComponent {

    SimpleList<Product> productList;
    ProductEditForm form;

    public ProductsPage() {
        form = new ProductEditForm();
        productList = new FormList<Product>(Product.class, new Object[] { "id",
                "name" }, form);
        final DefaultHorizontalLayout layout = new DefaultHorizontalLayout();
        layout.addComponents(productList, form);
        setCompositionRoot(layout);
    }

}