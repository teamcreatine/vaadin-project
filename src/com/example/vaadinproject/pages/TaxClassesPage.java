package com.example.vaadinproject.pages;

import com.example.vaadinproject.entities.TaxClass;
import com.example.vaadinproject.forms.SimpleForm;
import com.example.vaadinproject.forms.fields.FormList;
import com.example.vaadinproject.forms.fields.SimpleList;
import com.example.vaadinproject.layouts.DefaultHorizontalLayout;
import com.vaadin.ui.CustomComponent;

/**
 * Component for listing and editing tax classes.
 * 
 * @author Jukka Viljala <jupevi@utu.fi>
 * 
 */
public class TaxClassesPage extends CustomComponent {

    SimpleList<TaxClass> taxClassList;
    SimpleForm<TaxClass> form;

    public TaxClassesPage() {
        form = new SimpleForm<TaxClass>(TaxClass.class);
        taxClassList = new FormList<TaxClass>(TaxClass.class, new Object[] {
                "id", "name" }, form);
        DefaultHorizontalLayout layout = new DefaultHorizontalLayout();
        layout.addComponents(taxClassList, form);
        setCompositionRoot(layout);
    }
}
