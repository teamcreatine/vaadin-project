package com.example.vaadinproject.pages;

import com.example.vaadinproject.entities.Customer;
import com.example.vaadinproject.forms.SimpleForm;
import com.example.vaadinproject.forms.fields.FormList;
import com.example.vaadinproject.forms.fields.SimpleList;
import com.example.vaadinproject.layouts.DefaultHorizontalLayout;
import com.vaadin.ui.CustomComponent;

/**
 * Component for listing and editing customers.
 * 
 * @author Jukka Viljala <jupevi@utu.fi>
 * 
 */
public class CustomersPage extends CustomComponent {

    SimpleList<Customer> customerList;
    SimpleForm<Customer> form;

    public CustomersPage() {
        form = new SimpleForm<Customer>(Customer.class);
        customerList = new FormList<Customer>(Customer.class, new Object[] {
                "id", "name" }, form);
        DefaultHorizontalLayout layout = new DefaultHorizontalLayout();
        layout.addComponents(customerList, form);
        setCompositionRoot(layout);
    }

}
