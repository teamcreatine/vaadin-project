package com.example.vaadinproject;

import javax.servlet.annotation.WebServlet;

import com.example.vaadinproject.forms.SettingsForm;
import com.example.vaadinproject.pages.CurrenciesPage;
import com.example.vaadinproject.pages.CustomersPage;
import com.example.vaadinproject.pages.InvoicesPage;
import com.example.vaadinproject.pages.PaymentTermsPage;
import com.example.vaadinproject.pages.ProductsPage;
import com.example.vaadinproject.pages.SalesOrdersPage;
import com.example.vaadinproject.pages.SuppliersPage;
import com.example.vaadinproject.pages.TaxClassesPage;
import com.example.vaadinproject.windows.WindowManager;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.Command;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;
import com.vaadin.ui.Window.CloseEvent;
import com.vaadin.ui.Window.CloseListener;

@Title("Vaadin Project")
@Theme("vaadinproject")
/**
 * Main UI. Defines pages and handles showing windows and settings based on menu clicks.
 * 
 * @author Jukka Viljala <jupevi@utu.fi>
 *
 */
public class VaadinprojectUI extends UI {

    private MenuBar mainMenu;

    @WebServlet(value = "/*", asyncSupported = true)
    @VaadinServletConfiguration(productionMode = false, ui = VaadinprojectUI.class, widgetset = "com.example.vaadinproject.widgetset.VaadinprojectWidgetset")
    public static class Servlet extends VaadinServlet {
    }

    @Override
    protected void init(VaadinRequest request) {
        VaadinSession.getCurrent().setConverterFactory(new ConverterFactory());

        mainMenu = new MenuBar();

        initPages();
        makeMenu();

        if (!Settings.exists()) {
            setContent(new SettingsForm());
        } else {
            setContent(mainMenu);
        }
    }

    /**
     * Initializes menu links to pages.
     */
    private void initPages() {
        WindowManager.addPage("Currencies", CurrenciesPage.class);
        WindowManager.addPage("Tax Classes", TaxClassesPage.class);
        WindowManager.addPage("Payment Terms", PaymentTermsPage.class);
        WindowManager.addPage("Customers", CustomersPage.class);
        WindowManager.addPage("Suppliers", SuppliersPage.class);
        WindowManager.addPage("Products", ProductsPage.class);
        WindowManager.addPage("Sales Orders", SalesOrdersPage.class);
        WindowManager.addPage("Invoices", InvoicesPage.class);
    }

    /**
     * Makes the menu based on pages stored in WindowManager.
     */
    private void makeMenu() {
        // First add settings page
        mainMenu.addItem("Settings", new Command() {
            public void menuSelected(MenuItem selectedItem) {
                // Close open windows
                for (Window window : WindowManager.getWindows().values()) {
                    if (window != null) {
                        window.close();
                    }
                }
                setContent(new SettingsForm(new CloseListener() {
                    // Actually a cancel event, restore menu
                    public void windowClose(CloseEvent e) {
                        setContent(mainMenu);
                    }
                }));
            }
        });

        // Then everything else
        for (final String key : WindowManager.getPages().keySet()) {
            mainMenu.addItem(key, new Command() {
                public void menuSelected(MenuItem selectedItem) {
                    WindowManager.show(key);
                }
            });
        }
    }

}