package com.example.vaadinproject.forms;

import java.util.ArrayList;

import com.example.vaadinproject.entities.BaseEntity;
import com.example.vaadinproject.forms.FormListeners.CommitListener;
import com.example.vaadinproject.forms.FormListeners.ModifiedListener;
import com.example.vaadinproject.forms.FormListeners.RevertListener;
import com.example.vaadinproject.forms.annotations.FormField.FormFieldMetadata;
import com.example.vaadinproject.layouts.ButtonSet;
import com.vaadin.addon.jpacontainer.EntityItem;
import com.vaadin.data.Item;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup.CommitException;
import com.vaadin.data.util.BeanItem;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.event.FieldEvents.TextChangeNotifier;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Field;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.themes.Reindeer;

/**
 * Superclass for forms editing an entity. Creates fields using a factory and
 * includes buttons for saving and reverting changes if the form is buffered.
 * 
 * @author Jukka Viljala <jupevi@utu.fi>
 * 
 * @param <T>
 *            type of entity to edit
 */
public abstract class AbstractForm<T extends BaseEntity> extends
        CustomComponent {

    private final ArrayList<ModifiedListener> modifiedListeners = new ArrayList<ModifiedListener>();
    private final ArrayList<CommitListener> commitListeners = new ArrayList<CommitListener>();
    private final ArrayList<RevertListener> revertListeners = new ArrayList<RevertListener>();

    /**
     * Internal bool to tell value change handlers to ignore the event.
     */
    private boolean ignoreValueChange;

    private final Class<T> type;
    /**
     * Buffered forms aren't committed until asked to. Defaults to true.
     */
    private boolean buffered;

    protected FormFactory factory;
    protected FieldGroup binder;

    protected Button saveButton;
    protected Button revertButton;
    protected ButtonSet buttons;

    /**
     * Creates a buffered form and the fields specified by the entity.
     * 
     * @param type
     *            type of entity to edit
     */
    public AbstractForm(Class<T> type) {
        this(type, true);
    }

    /**
     * Creates a form and the fields specified by the entity.
     * 
     * @param type
     *            type of entity to edit
     * @param buffered
     *            whether wait until commit command to persist changes
     */
    public AbstractForm(Class<T> type, boolean buffered) {
        this.type = type;

        factory = new FormFactory(type);
        binder = new FieldGroup();
        setBuffered(buffered);

        factory.createFields();

        if (buffered) {
            saveButton = new Button("Save");
            revertButton = new Button("Revert");
            buttons = new ButtonSet(saveButton, revertButton);
            buttons.setEnabled(false);

            // Might be a bad idea here, but works with windows
            saveButton.setClickShortcut(KeyCode.ENTER);
            saveButton.addStyleName(Reindeer.BUTTON_DEFAULT);

            initButtonListeners();
        }

        setEnabled(false);
    }

    /**
     * Initializes listeners for buttons.
     */
    private void initButtonListeners() {
        saveButton.addClickListener(new ClickListener() {
            public void buttonClick(ClickEvent event) {
                commit();
            }
        });

        revertButton.addClickListener(new ClickListener() {
            public void buttonClick(ClickEvent event) {
                revert();
            }
        });
    }

    /**
     * Initializes FieldGroup by binding factory created fields to their correct
     * property ids. Also adds value change listeners if needed.
     */
    protected void initBindings() {
        for (Field<?> field : factory.getFields()) {
            FormFieldMetadata metadata = factory.getMetadata(field.getId());
            if (metadata.shouldBind()) {
                binder.bind(field, field.getId());
            }
            if (buffered && metadata.doesFireModifiedEvents()) {
                initFieldChangeListener(field);
            }
        }
        binder.setItemDataSource(getEmpty());
    }

    /**
     * Initializes value change listener for field. Uses TextChangeListener for
     * types implementing TextChangeNotifier, otherwise uses
     * ValueChangeListener.
     * 
     * @param field
     *            field to add listener to
     */
    protected void initFieldChangeListener(final Field<?> field) {
        if (field == null) {
            return;
        }
        if (field instanceof TextChangeNotifier) {
            ((TextChangeNotifier) field)
                    .addTextChangeListener(new TextChangeListener() {
                        public void textChange(TextChangeEvent event) {
                            modified(field);
                        }
                    });
        } else {
            field.addValueChangeListener(new ValueChangeListener() {
                public void valueChange(ValueChangeEvent event) {
                    modified(field);
                }
            });
        }
    }

    /**
     * Registers a listener to receive form modified events.
     * 
     * @param listener
     *            listener
     */
    public void addModifiedListener(ModifiedListener listener) {
        modifiedListeners.add(listener);
    }

    /**
     * Registers a listener to receive form commit events.
     * 
     * @param listener
     *            listener
     */
    public void addCommitListener(CommitListener listener) {
        commitListeners.add(listener);
    }

    /**
     * Registers a listener to receive form revert events.
     * 
     * @param listener
     *            listener
     */
    public void addRevertListener(RevertListener listener) {
        revertListeners.add(listener);
    }

    /**
     * Handles enabling buttons and notifying modified listeners.
     * 
     * @param field
     *            modified field
     */
    protected void modified(Field<?> field) {
        if (ignoreValueChange || !buffered) {
            return;
        }
        if (!saveButton.isEnabled()) {
            buttons.setEnabled(true);
        }
        for (ModifiedListener listener : modifiedListeners) {
            listener.modified(field);
        }
    }

    /**
     * Gets whether form is modified.
     * 
     * @return true if modified
     */
    public boolean isModified() {
        return buffered && saveButton.isEnabled();
    }

    /**
     * Commits changes to FieldGroup and notifies commit listeners.
     */
    final public void commit() {
        if (!buffered) {
            return;
        }
        onCommit();
        try {
            binder.commit();
            buttons.setEnabled(false);
            for (CommitListener listener : commitListeners) {
                listener.commit(getItemId());
            }
        } catch (CommitException e) {
            Notification.show("Unable to commit changes!", Type.ERROR_MESSAGE);
            e.printStackTrace();
        }
    }

    /**
     * Reverts changes in FieldGroup and notifies revert listeners.
     */
    final public void revert() {
        if (!buffered) {
            return;
        }
        onRevert();
        binder.discard();
        buttons.setEnabled(false);
        for (RevertListener listener : revertListeners) {
            listener.revert(getItemId());
        }
    }

    /**
     * Sets the entity to edit. Note that it needs to be wrapped in an Item.
     * 
     * @param item
     *            item to edit
     */
    final public void setItem(Item item) {
        ignoreValueChange = true;
        onSetItem(item);
        binder.setItemDataSource(item);
        if (item.getItemProperty("id").getValue().equals(0)) {
            setEnabled(false);
        } else {
            setEnabled(true);
        }
        if (buffered) {
            buttons.setEnabled(false);
        }
        ignoreValueChange = false;
    }

    /**
     * Virtual method for subclasses to override.
     */
    protected void onCommit() {

    }

    /**
     * Virtual method for subclasses to override.
     */
    protected void onRevert() {

    }

    /**
     * Virtual method for subclasses to override.
     */
    protected void onSetItem(Item item) {

    }

    @SuppressWarnings("unchecked")
    /**
     * Gets the current entity wrapped in an EntityItem.
     * 
     * @return current entity wrapped in EntityItem
     */
    final protected EntityItem<T> getItem() {
        return (EntityItem<T>) binder.getItemDataSource();
    }

    /**
     * Gets the current entity.
     * 
     * @return current entity
     */
    final protected T getEntity() {
        return getItem().getEntity();
    }

    /**
     * Gets the current item id.
     * 
     * @return current item id
     */
    final protected Object getItemId() {
        return getItem().getItemId();
    }

    /**
     * Gets an empty instance of the editable entity. Used for resetting the
     * form.
     * 
     * @return empty instance wrapped in a BeanItem
     */
    public Item getEmpty() {
        try {
            return new BeanItem<T>(type.newInstance());
        } catch (InstantiationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Gets whether value changes should be ignored.
     * 
     * @return true if they should
     */
    public boolean isIgnoreValueChange() {
        return ignoreValueChange;
    }

    /**
     * Gets whether the form is buffered.
     * 
     * @return true if buffered
     */
    public boolean isBuffered() {
        return buffered;
    }

    /**
     * Sets form buffered state.
     * 
     * @param buffered
     *            true to buffer changes
     */
    public void setBuffered(boolean buffered) {
        this.buffered = buffered;
        this.binder.setBuffered(buffered);
    }

}
