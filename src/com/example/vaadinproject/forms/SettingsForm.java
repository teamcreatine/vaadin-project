package com.example.vaadinproject.forms;

import java.util.ArrayList;
import java.util.List;

import com.example.vaadinproject.Setting;
import com.example.vaadinproject.Settings;
import com.example.vaadinproject.forms.fields.FormTextField;
import com.example.vaadinproject.layouts.ButtonSet;
import com.example.vaadinproject.layouts.DefaultFormLayout;
import com.example.vaadinproject.layouts.DefaultVerticalLayout;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Window.CloseListener;
import com.vaadin.ui.themes.Reindeer;

/**
 * Barebones form for storing settings in a file.
 * 
 * @author Jukka Viljala <jupevi@utu.fi>
 * 
 */
public class SettingsForm extends CustomComponent {

    private final CloseListener closeListener;

    DefaultVerticalLayout layout;
    DefaultFormLayout formLayout;
    List<FormTextField> fields;

    Button acceptButton;
    Button cancelButton;

    public SettingsForm() {
        this(null);
    }

    public SettingsForm(CloseListener closeListener) {
        this.closeListener = closeListener;

        layout = new DefaultVerticalLayout();
        formLayout = new DefaultFormLayout();
        fields = new ArrayList<FormTextField>();

        acceptButton = new Button("Accept");
        cancelButton = new Button("Cancel");

        initLayout();
        initListeners();
    }

    void initLayout() {
        setSizeFull();
        layout.setSizeFull();

        for (Setting setting : Settings.SETTINGS) {
            FormTextField field = new FormTextField(setting.getName(),
                    setting.getProperty(), 128, 200);
            field.setValue(setting.getValue());
            fields.add(field);
            formLayout.addComponent(field);
        }

        acceptButton.setClickShortcut(KeyCode.ENTER);
        acceptButton.addStyleName(Reindeer.BUTTON_DEFAULT);
        ButtonSet buttons = new ButtonSet(acceptButton);
        if (Settings.exists()) {
            buttons.addComponent(cancelButton);
        }

        formLayout.addComponent(buttons);

        Panel panel = new Panel("Database Settings", formLayout);
        panel.setSizeUndefined();
        layout.addComponent(panel);
        layout.setComponentAlignment(panel, Alignment.TOP_CENTER);
        setCompositionRoot(layout);
    }

    void initListeners() {
        acceptButton.addClickListener(new ClickListener() {
            public void buttonClick(ClickEvent event) {
                getUI().getPage().reload();
                store();
            }
        });

        cancelButton.addClickListener(new ClickListener() {
            public void buttonClick(ClickEvent event) {
                closeListener.windowClose(null);
            }
        });
    }

    public void store() {
        for (FormTextField field : fields) {
            Settings.set(field.getId(), field.getValue());
        }
        Settings.store();
    }

}
