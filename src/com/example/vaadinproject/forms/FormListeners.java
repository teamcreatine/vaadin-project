package com.example.vaadinproject.forms;

import com.vaadin.ui.Field;

/**
 * Interface specifying form event listeners.
 * 
 * @author Jukka Viljala <jupevi@utu.fi>
 * 
 */
public interface FormListeners {

    public interface ModifiedListener {
        public void modified(Field<?> field);
    }

    public interface CommitListener {
        public void commit(Object itemId);
    }

    public interface RevertListener {
        public void revert(Object itemId);
    }

    public interface AddListener {
        public void beforeAdd(Object obj);
    }

}
