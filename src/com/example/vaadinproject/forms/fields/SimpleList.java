package com.example.vaadinproject.forms.fields;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import org.vaadin.resetbuttonfortextfield.ResetButtonForTextField;

import com.example.vaadinproject.layouts.DefaultVerticalLayout;
import com.example.vaadinproject.util.JPAHelper;
import com.example.vaadinproject.util.StringUtils;
import com.vaadin.addon.jpacontainer.EntityItem;
import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.addon.jpacontainer.JPAContainerItem;
import com.vaadin.data.Container.Filter;
import com.vaadin.data.Container.ItemSetChangeEvent;
import com.vaadin.data.Container.ItemSetChangeListener;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.filter.Or;
import com.vaadin.data.util.filter.SimpleStringFilter;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.ui.AbstractTextField.TextChangeEventMode;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Table;

/**
 * Shows list of entities. Allows filtering the list using a text field.
 * 
 * @author Jukka Viljala <jupevi@utu.fi>
 * 
 * @param <T>
 *            type of entity to list
 */
public class SimpleList<T> extends CustomComponent implements Serializable {

    protected final Class<T> type;
    protected Object[] visibleColumns;

    DefaultVerticalLayout layout;
    JPAContainer<T> container;
    Table list;
    FormTextField filterField;

    /**
     * Creates a new list for entity type.
     * 
     * @param type
     *            entity type
     * @param visibleColumns
     *            visible columns
     */
    public SimpleList(Class<T> type, Object[] visibleColumns) {
        this.type = type;
        this.visibleColumns = visibleColumns;

        init();
        initLayout();
        initInternalListeners();
        initBindings();
    }

    /**
     * Initializes variables.
     */
    protected void init() {
        container = JPAHelper.getContainer(type);
        container.setApplyFiltersImmediately(false);

        list = new Table();
        filterField = new FormTextField(null, "filter", 200, 250);
        filterField.setTextChangeEventMode(TextChangeEventMode.LAZY);
        filterField.setTextChangeTimeout(350);
    }

    /**
     * Initializes layout.
     */
    protected void initLayout() {
        layout = new DefaultVerticalLayout();

        // list.setNullSelectionAllowed(false);
        list.setSelectable(true);
        list.setImmediate(true);
        list.setWidth(250, Unit.PIXELS);

        filterField.setInputPrompt("Filter by "
                + StringUtils.join(visibleColumns));
        ResetButtonForTextField.extend(filterField);

        layout.addComponents(list, filterField);

        setCompositionRoot(layout);
    }

    /**
     * Initializes internal listeners.
     */
    private void initInternalListeners() {
        list.addItemSetChangeListener(new ItemSetChangeListener() {
            public void containerItemSetChange(ItemSetChangeEvent event) {
                updateCaption();
            }
        });

        list.addValueChangeListener(new ValueChangeListener() {
            public void valueChange(ValueChangeEvent event) {
                updateCaption();
            }
        });

        filterField.addTextChangeListener(new TextChangeListener() {
            public void textChange(TextChangeEvent event) {
                container.removeAllContainerFilters();
                Filter[] filters = new Filter[visibleColumns.length];
                int i = 0;
                for (Object column : visibleColumns) {
                    filters[i++] = new SimpleStringFilter(column, event
                            .getText(), true, false);
                }
                container.addContainerFilter(new Or(filters));
                container.applyFilters();
            }
        });
    }

    /**
     * Binds the list to the entity container.
     */
    protected void initBindings() {
        list.setContainerDataSource(container);
        list.setVisibleColumns(visibleColumns);
        list.setColumnWidth("id", 40);
        updateCaption();
    }

    /**
     * Updates caption to show number of items.
     */
    protected void updateCaption() {
        int selected;
        if (!isMultiSelect()) {
            selected = (list.getValue() == null ? 0 : 1);
        } else {
            selected = ((Set<?>) list.getValue()).size();
        }
        list.setCaption(String.format("%d item(s) visible, %s selected",
                container.size(), selected));
    }

    public Object[] getVisibleColumns() {
        return visibleColumns;
    }

    public void setVisibleColumns(Object[] visibleColumns) {
        this.visibleColumns = visibleColumns;
    }

    public JPAContainer<T> getContainer() {
        return container;
    }

    /**
     * Gets selected value of list.
     * 
     * @return selected value
     */
    public Object getValue() {
        return list.getValue();
    }

    /**
     * Gets selected entity wrapped in a JPAContainerItem.
     * 
     * @return selected item
     */
    public JPAContainerItem<T> getSelectedItem() {
        return (JPAContainerItem<T>) container.getItem(list.getValue());
    }

    @SuppressWarnings("unchecked")
    /**
     * Gets set of selected entities wrapped in a JPAContainerItem.
     *  
     * @return set of selected items
     */
    public Set<JPAContainerItem<T>> getSelectedItems() {
        Set<JPAContainerItem<T>> set = new HashSet<JPAContainerItem<T>>();
        Object value = list.getValue();
        if (!isMultiSelect()) {
            set.add((JPAContainerItem<T>) container.getItem(value));
        } else {
            for (Object val : (Set<Object>) value) {
                set.add((JPAContainerItem<T>) container.getItem(val));
            }
        }
        return set;
    }

    /**
     * Gets selected entity instance.
     * 
     * @return selected entity
     */
    public T getSelectedEntity() {
        EntityItem<T> item;
        if (!isMultiSelect()) {
            item = container.getItem(list.getValue());
        } else {
            Object itemId = ((Set<?>) list.getValue()).iterator().next();
            item = container.getItem(itemId);
        }
        if (item != null) {
            return item.getEntity();
        }
        return null;
    }

    /**
     * Gets set of selected entity instances.
     * 
     * @return set of entities
     */
    public Set<T> getSelectedEntities() {
        Set<T> set = new HashSet<T>();
        if (!isMultiSelect()) {
            EntityItem<T> item = container.getItem(list.getValue());
            if (item != null) {
                set.add(item.getEntity());
            }
        } else {
            Set<?> itemIds = (Set<?>) list.getValue();
            for (Object itemId : itemIds) {
                EntityItem<T> item = container.getItem(itemId);
                if (item != null) {
                    set.add(item.getEntity());
                }
            }
        }
        return set;
    }

    public boolean isMultiSelect() {
        return list.isMultiSelect();
    }

    public void setMultiSelect(boolean enabled) {
        list.setMultiSelect(enabled);
    }

    public void select(Object itemId) {
        list.select(itemId);
    }

    public void addItemClickListener(ItemClickListener listener) {
        list.addItemClickListener(listener);
    }

}
