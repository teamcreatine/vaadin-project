package com.example.vaadinproject.forms.fields;

import java.text.NumberFormat;
import java.util.Locale;

import com.example.vaadinproject.util.StringUtils;
import com.vaadin.server.UserError;

/**
 * Class for an integer input field.
 * 
 * @author Jukka Viljala <jupevi@utu.fi>
 * 
 */
public class FormIntegerField extends AbstractNumberField<Integer> {

    /**
     * Used for parsing and formatting value.
     */
    private final NumberFormat numberFormat;

    /**
     * Creates a new integer field. Infers property id from caption.
     * 
     * @param caption
     *            field caption
     * @param fieldWidth
     *            field width
     */
    public FormIntegerField(String caption, int fieldWidth) {
        this(caption, StringUtils.toCamelCase(caption), fieldWidth);
    }

    /**
     * Creates a new integer field.
     * 
     * @param caption
     *            field caption
     * @param propertyId
     *            property id
     * @param fieldWidth
     *            field width
     */
    public FormIntegerField(String caption, String propertyId, int fieldWidth) {
        super(caption, propertyId, fieldWidth);

        NumberFormat format = NumberFormat.getIntegerInstance(Locale
                .getDefault());

        this.numberFormat = format;
        this.textField.setMaxLength(10);
    }

    @Override
    public Class<Integer> getType() {
        return Integer.class;
    }

    @Override
    public void setValue(String value) {
        setComponentError(null);
        Integer number = null;
        try {
            number = Integer.parseInt(value);
        } catch (Exception e) {
            setComponentError(new UserError("\"" + value
                    + "\" is not a valid integer value"));
        }
        super.setValue(number);
    }

    @Override
    public NumberFormat getNumberFormat() {
        return numberFormat;
    }

}
