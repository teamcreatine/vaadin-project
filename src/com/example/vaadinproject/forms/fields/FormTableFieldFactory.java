package com.example.vaadinproject.forms.fields;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.example.vaadinproject.entities.TableRowEntity;
import com.example.vaadinproject.forms.FormFactory;
import com.vaadin.addon.jpacontainer.EntityItem;
import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.data.Container;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.event.ShortcutAction.ModifierKey;
import com.vaadin.event.ShortcutListener;
import com.vaadin.ui.AbstractField;
import com.vaadin.ui.Component;
import com.vaadin.ui.Field;
import com.vaadin.ui.HasComponents;
import com.vaadin.ui.Table;
import com.vaadin.ui.TableFieldFactory;

import elemental.events.KeyboardEvent.KeyCode;

/**
 * Table input field factory. Uses an ordinary FormFactory but handles caching
 * on its own. Supports keyboard navigation between cells and fires cell value
 * change events for columns.
 * 
 * @author Jukka Viljala <jupevi@utu.fi>
 * 
 */
public class FormTableFieldFactory implements TableFieldFactory {

    private int columnCount;
    private Object lastItemId;

    private JPAContainer<?> container;

    private final Map<Object, CellValueChangeListener> valueChangeListeners;

    private final Table table;
    private final FormFactory factory;

    private final Map<Object, HashMap<Object, AbstractField<?>>> fieldCache;
    private final Map<Integer, HashMap<Integer, AbstractField<?>>> rows;
    private final Map<Object, ArrayList<AbstractField<?>>> columns;

    /**
     * Creates a new table input field factory for entity type.
     * 
     * @param type
     *            type of entity edited in table
     */
    public FormTableFieldFactory(Class<? extends TableRowEntity> type,
            Table table) {
        this.valueChangeListeners = new HashMap<Object, CellValueChangeListener>();
        this.table = table;
        this.factory = new FormFactory(type);
        this.fieldCache = new HashMap<Object, HashMap<Object, AbstractField<?>>>();
        this.columns = new HashMap<Object, ArrayList<AbstractField<?>>>();
        this.rows = new HashMap<Integer, HashMap<Integer, AbstractField<?>>>();
    }

    @Override
    /**
     * Creates input field using the annotations defined in the entity.
     * Keeps track of fields in three different kinds of maps for easy access later.
     * 
     * Huge resource hog because the table wants to create a new field for every cell any time anything changes.
     * Especially since the maps are cleared often.
     */
    public Field<?> createField(Container container, Object itemId,
            Object propertyId, Component uiContext) {
        if (this.container == null) {
            this.container = (JPAContainer<?>) container;
        }
        if (lastItemId != null && itemId != lastItemId) {
            columnCount = 0;
        }
        lastItemId = itemId;
        int rowIndex = ((Container.Indexed) container).indexOfId(itemId);

        // Add new row
        if (!rows.containsKey(rowIndex)) {
            rows.put(rowIndex, new HashMap<Integer, AbstractField<?>>());
        }
        // Add new column
        if (!columns.containsKey(propertyId)) {
            columns.put(propertyId, new ArrayList<AbstractField<?>>());
        }

        AbstractField<?> field = null;
        if (fieldCache.containsKey(itemId)) {
            if (fieldCache.get(itemId) != null
                    && fieldCache.get(itemId).containsKey(propertyId)) {
                // Use cached field for cell
                field = fieldCache.get(itemId).get(propertyId);
            }
        }
        if (field == null) {
            field = (AbstractField<?>) factory.createField(
                    propertyId.toString(), true);
            if (field == null) {
                // Unable to create field using factory, bail out
                return null;
            }
            // Add to field map
            if (!fieldCache.containsKey(itemId)) {
                fieldCache.put(itemId, new HashMap<Object, AbstractField<?>>());
            }
            if (valueChangeListeners.containsKey(propertyId)) {
                // Column has a value change listener
                addCellValueChangeListenerToField(field,
                        valueChangeListeners.get(propertyId));
            }
            fieldCache.get(itemId).put(propertyId, field);
        }

        // Add to row access map
        rows.get(rowIndex).put(columnCount, field);
        // Add to column access map
        columns.get(propertyId).add(field);
        // Set last value so we know when the user changes it
        field.setData(container.getItem(itemId).getItemProperty(propertyId)
                .getValue());
        // Set id so we know where this cell is
        field.setId(rowIndex + "," + columnCount);
        columnCount++;
        return field;
    }

    /**
     * Adds a new cell value change listener for a column. Each cell in that
     * column fires the event.
     * 
     * @param column
     *            column
     * @param listener
     *            listener
     */
    public void addCellValueChangeListener(String column,
            final CellValueChangeListener listener) {
        valueChangeListeners.put(column, listener);
        List<AbstractField<?>> list = columns.get(column);
        if (list != null) {
            for (AbstractField<?> field : list) {
                addCellValueChangeListenerToField(field, listener);
            }
        }
    }

    /**
     * Internally adds cell value change listener to field.
     * 
     * @param field
     *            field
     * @param listener
     *            listener
     */
    private void addCellValueChangeListenerToField(
            final AbstractField<?> field, final CellValueChangeListener listener) {
        field.addValueChangeListener(new ValueChangeListener() {
            public void valueChange(ValueChangeEvent event) {
                Property<?> prop = event.getProperty();
                Object origData = field.getData();
                Object newData = prop.getValue();
                boolean userChanged = false;
                // Try to determine if the user changed the data
                if (origData == null && newData != null) {
                    userChanged = !newData.equals(origData);
                } else if (origData != null) {
                    userChanged = !origData.equals(newData);
                }
                listener.cellValueChange(new CellValueChangeEvent(userChanged,
                        field, prop));
            }
        });
    }

    /**
     * Adds keyboard navigation support for the table. Allows using Ctrl+arrow
     * keys to navigate cells.
     * 
     * @param table
     *            table to add listener for
     */
    public void addShortcutListenerToTable() {
        table.addShortcutListener(new ShortcutListener(null, KeyCode.RIGHT,
                new int[] { ModifierKey.CTRL }) {
            @Override
            public void handleAction(Object sender, Object target) {
                if (target instanceof AbstractField) {
                    handleKeyNavigation((AbstractField<?>) target, 0, 1);
                }
            }
        });
        table.addShortcutListener(new ShortcutListener(null, KeyCode.LEFT,
                new int[] { ModifierKey.CTRL }) {
            @Override
            public void handleAction(Object sender, Object target) {
                if (target instanceof AbstractField) {
                    handleKeyNavigation((AbstractField<?>) target, 0, -1);
                }
            }
        });
        table.addShortcutListener(new ShortcutListener(null, KeyCode.DOWN,
                new int[] { ModifierKey.CTRL }) {
            @Override
            public void handleAction(Object sender, Object target) {
                if (target instanceof AbstractField) {
                    handleKeyNavigation((AbstractField<?>) target, 1, 0);
                }
            }
        });
        table.addShortcutListener(new ShortcutListener(null, KeyCode.UP,
                new int[] { ModifierKey.CTRL }) {
            @Override
            public void handleAction(Object sender, Object target) {
                if (target instanceof AbstractField) {
                    handleKeyNavigation((AbstractField<?>) target, -1, 0);
                }
            }
        });
    }

    /**
     * Handles keyboard navigation within a table. Moves focus by offsets.
     * 
     * @param field
     *            field that fired the action event
     * @param table
     *            table the field belongs in
     * @param rowOffset
     *            row offset
     * @param cellOffset
     *            cell offset
     */
    private void handleKeyNavigation(AbstractField<?> field, int rowOffset,
            int cellOffset) {
        HasComponents parent = field.getParent();
        // Hack because the text field component of a number field
        // sends the event, instead of the number field itself
        if (parent instanceof AbstractNumberField) {
            parent = parent.getParent();
        }
        if (parent.equals(table)) {
            int[] loc = getCellLocation(field);
            int newRowIndex = loc[0] + rowOffset;
            int newColIndex = loc[1] + cellOffset;
            int curFirstIndex = table.getCurrentPageFirstItemIndex();
            int curLastIndex = curFirstIndex + table.getPageLength();
            if (newRowIndex >= curFirstIndex && newRowIndex < curLastIndex
                    && newRowIndex < table.size()) {
                // Get row with offset
                HashMap<Integer, AbstractField<?>> row = rows.get(newRowIndex);
                if (row != null) {
                    // Get cell with offset
                    AbstractField<?> nextField = row.get(newColIndex);
                    if (nextField != null) {
                        nextField.focus();
                    }
                }
            } else {
                /*
                 * Would be nice if this worked but it has some weird issues
                 * where the fields don't get rendered at all, especially when
                 * trying to move from the first page to the next. Converter
                 * tries to get some odd values as well, using BaseEntity as
                 * target type.
                 */
                // table.setCurrentPageFirstItemIndex(newRowIndex);
            }
        }
    }

    /**
     * Resets columnt count and last item id.
     */
    public void reset() {
        columnCount = 0;
        lastItemId = null;
    }

    /**
     * Gets the row and column indices of a field, or null of not available.
     * 
     * @param field
     *            field
     * @return array where 0th index is the row and 1st is the column
     */
    public static int[] getCellLocation(AbstractField<?> field) {
        if (field.getId() != null) {
            String[] loc = field.getId().split(",");
            if (loc.length == 2) {
                int rowIndex = Integer.parseInt(loc[0]);
                int cellIndex = Integer.parseInt(loc[1]);
                return new int[] { rowIndex, cellIndex };
            }
        }
        return null;
    }

    /**
     * Listener for cell value changes.
     * 
     * @author Jukka Viljala <jupevi@utu.fi>
     * 
     */
    public interface CellValueChangeListener extends Serializable {
        public void cellValueChange(CellValueChangeEvent event);
    }

    /**
     * Class containing cell value change information.
     * 
     * @author Jukka Viljala <jupevi@utu.fi>
     * 
     */
    public class CellValueChangeEvent {

        private final boolean userChanged;
        private final Property<?> property;
        private final EntityItem<?> item;

        private final AbstractField<?> field;
        private final int rowIndex;
        private final int columnIndex;

        public CellValueChangeEvent(boolean userChanged,
                AbstractField<?> field, Property<?> property) {
            this.userChanged = userChanged;
            this.property = property;
            this.field = field;

            int[] loc = FormTableFieldFactory.getCellLocation(field);
            this.rowIndex = loc[0];
            this.columnIndex = loc[1];
            Object itemId = FormTableFieldFactory.this.container
                    .getIdByIndex(rowIndex);
            this.item = FormTableFieldFactory.this.container.getItem(itemId);
        }

        public AbstractField<?> getField() {
            return field;
        }

        public boolean isUserChanged() {
            return userChanged;
        }

        public Property<?> getProperty() {
            return property;
        }

        public EntityItem<?> getItem() {
            return item;
        }

        public int getRowIndex() {
            return rowIndex;
        }

        public int getColumnIndex() {
            return columnIndex;
        }

    }

}
