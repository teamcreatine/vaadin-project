package com.example.vaadinproject.forms.fields;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

import com.example.vaadinproject.util.StringUtils;
import com.vaadin.server.UserError;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;

/**
 * Class for a decimal input field.
 * 
 * @author Jukka Viljala <jupevi@utu.fi>
 * 
 */
public class FormDecimalField extends AbstractNumberField<BigDecimal> {

    /**
     * Used for parsing and formatting value.
     */
    private final NumberFormat numberFormat;

    /**
     * Creates a new decimal field. Infers property id from caption.
     * 
     * @param caption
     *            field caption
     * @param fieldWidth
     *            field width
     */
    public FormDecimalField(String caption, int fieldWidth) {
        this(caption, StringUtils.toCamelCase(caption), fieldWidth);
    }

    /**
     * Creates a new decimal field.
     * 
     * @param caption
     *            field caption
     * @param propertyId
     *            property id
     * @param fieldWidth
     *            field width
     */
    public FormDecimalField(String caption, String propertyId, int fieldWidth) {
        super(caption, propertyId, fieldWidth);

        DecimalFormat format = (DecimalFormat) DecimalFormat
                .getNumberInstance(Locale.getDefault());
        format.setParseBigDecimal(true);
        format.setMinimumFractionDigits(2);
        format.setMaximumFractionDigits(4);
        format.setRoundingMode(RoundingMode.HALF_EVEN);

        this.numberFormat = format;
        this.textField.setMaxLength(20);
        this.textField.setInputPrompt(format.format(0.0));
    }

    @Override
    public Class<BigDecimal> getType() {
        return BigDecimal.class;
    }

    @Override
    public void setValue(String value) {
        setComponentError(null);
        if (value == null) {
            super.setValue((BigDecimal) null);
        } else {
            try {
                BigDecimal number = (BigDecimal) numberFormat.parse(value);
                super.setValue(number);
            } catch (ParseException e) {
                super.setValue((BigDecimal) null);
                setComponentError(new UserError("\"" + value
                        + "\" is not a valid decimal value"));
                Notification.show("Not a valid decimal value",
                        Type.TRAY_NOTIFICATION);
            }
        }
    }

    @Override
    public NumberFormat getNumberFormat() {
        return numberFormat;
    }

}
