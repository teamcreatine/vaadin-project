package com.example.vaadinproject.forms.fields;

import java.text.NumberFormat;

import com.example.vaadinproject.util.StringUtils;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.event.FieldEvents.TextChangeNotifier;
import com.vaadin.ui.AbstractTextField.TextChangeEventMode;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomField;
import com.vaadin.ui.TextField;

/**
 * Abstract class for handling numeric input. Uses a textfield internally.
 * 
 * @author Jukka Viljala <jupevi@utu.fi>
 * 
 * @param <T>
 *            type of input to handle (Integer, BigDecimal etc.)
 */
public abstract class AbstractNumberField<T extends Number> extends
        CustomField<T> implements TextChangeNotifier {

    protected TextField textField;

    /**
     * Used to ignore recursive value change events.
     */
    protected int recursions = 0;

    /**
     * Creates a new number field. Property id is inferred from caption.
     * 
     * @param caption
     *            field caption
     * @param fieldWidth
     *            field width
     */
    public AbstractNumberField(String caption, int fieldWidth) {
        this(caption, StringUtils.toCamelCase(caption), fieldWidth);
    }

    /**
     * Creates a new number field.
     * 
     * @param caption
     *            field caption
     * @param propertyId
     *            field property id
     * @param fieldWidth
     *            field width
     */
    public AbstractNumberField(String caption, String propertyId, int fieldWidth) {
        textField = new TextField(caption);
        if (fieldWidth > 0) {
            textField.setWidth(fieldWidth, Unit.PIXELS);
        }
        textField.setNullSettingAllowed(true);
        textField.setNullRepresentation("");
        textField.setTextChangeEventMode(TextChangeEventMode.TIMEOUT);
        textField.setTextChangeTimeout(50);

        this.setCaption(caption);
        this.setId(propertyId);

        init();
    }

    /**
     * Initializes field.
     */
    void init() {
        // Validate and format value on change
        textField.addValueChangeListener(new ValueChangeListener() {
            public void valueChange(
                    com.vaadin.data.Property.ValueChangeEvent event) {
                if (recursions == 0) {
                    recursions++;
                    try {
                        if (event.getProperty().getValue() == null) {
                            AbstractNumberField.this.setValue(null);
                        } else {
                            AbstractNumberField.this.setValue((String) event
                                    .getProperty().getValue());
                            if (AbstractNumberField.this.getValue() == null) {
                                textField.setValue(null);
                            } else {
                                textField.setValue(getNumberFormat().format(
                                        AbstractNumberField.this.getValue()));
                            }
                        }
                    } finally {
                        recursions--;
                    }
                }
            }
        });

        setImmediate(true);
    }

    @Override
    protected Component initContent() {
        return textField;
    }

    @Override
    protected void setInternalValue(T newValue) {
        super.setInternalValue(newValue);

        recursions++;
        try {
            if (newValue == null) {
                textField.setValue(null);
            } else {
                textField.setValue(getNumberFormat().format(newValue));
            }
        } finally {
            recursions--;
        }
    }

    @Override
    public void setId(String id) {
        super.setId(id);
        textField.setId(id);
    }

    @Override
    public void setImmediate(boolean immediate) {
        super.setImmediate(immediate);
        textField.setImmediate(immediate);
    }

    @Override
    public void setSizeFull() {
        super.setSizeFull();
        textField.setSizeFull();
    }

    @Override
    public void setSizeUndefined() {
        super.setSizeUndefined();
        textField.setSizeUndefined();
    }

    @Override
    public void setWidth(String width) {
        super.setWidth(width);
        textField.setWidth(width);
    }

    @Override
    public void setHeight(String height) {
        super.setHeight(height);
        textField.setHeight(height);
    }

    @Override
    public void focus() {
        textField.focus();
    }

    public void selectAll() {
        textField.selectAll();
    }

    @Override
    public void addTextChangeListener(TextChangeListener listener) {
        textField.addTextChangeListener(listener);
    }

    @Override
    public void addListener(TextChangeListener listener) {

    }

    @Override
    public void removeTextChangeListener(TextChangeListener listener) {
        textField.removeTextChangeListener(listener);
    }

    @Override
    public void removeListener(TextChangeListener listener) {

    }

    /**
     * Number format used for formatting the displayd value.
     * 
     * @return number format instance
     */
    public abstract NumberFormat getNumberFormat();

    public abstract void setValue(String value);

}
