package com.example.vaadinproject.forms.fields;

import com.example.vaadinproject.util.StringUtils;
import com.vaadin.ui.NativeSelect;

/**
 * Simple class for a select box.
 * 
 * @author Jukka Viljala <jupevi@utu.fi>
 * 
 */
public class FormNativeSelect extends NativeSelect {

    /**
     * Creates a new select box. Infers property id from caption.
     * 
     * @param caption
     *            field caption
     * @param fieldWidth
     *            field width
     */
    public FormNativeSelect(String caption, int fieldWidth) {
        this(caption, StringUtils.toCamelCase(caption), fieldWidth);
    }

    /**
     * Creates a new select box.
     * 
     * @param caption
     *            field caption
     * @param propertyId
     *            property id
     * @param fieldWidth
     *            field width
     */
    public FormNativeSelect(String caption, String propertyId, int fieldWidth) {
        this.setCaption(caption);
        this.setId(propertyId);
        if (fieldWidth > 0) {
            this.setWidth(fieldWidth, Unit.PIXELS);
        }
        this.setImmediate(true);
    }

}
