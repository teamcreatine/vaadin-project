package com.example.vaadinproject.forms.fields;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.PersistenceException;

import org.vaadin.dialogs.ConfirmDialog;

import com.example.vaadinproject.entities.BaseEntity;
import com.example.vaadinproject.forms.AbstractForm;
import com.example.vaadinproject.forms.FormListeners.AddListener;
import com.example.vaadinproject.forms.FormListeners.CommitListener;
import com.example.vaadinproject.forms.FormListeners.ModifiedListener;
import com.example.vaadinproject.forms.FormListeners.RevertListener;
import com.example.vaadinproject.layouts.ButtonSet;
import com.example.vaadinproject.util.StringUtils;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Field;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.UI;

/**
 * Displays a list of items that are connected to an editor form. Allows editing
 * the list.
 * 
 * @author Jukka Viljala <jupevi@utu.fi>
 * 
 * @param <T>
 *            type of entity to list and edit
 */
public class FormList<T extends BaseEntity> extends SimpleList<T> {

    private final List<AddListener> addListeners = new ArrayList<AddListener>();

    AbstractForm<T> form;
    Button addButton;
    Button deleteButton;

    /**
     * Creates a new form list.
     * 
     * @param type
     *            type of entity
     * @param visibleColumns
     *            visible columns
     * @param form
     *            connected form
     */
    public FormList(Class<T> type, Object[] visibleColumns, AbstractForm<T> form) {
        super(type, visibleColumns);

        this.form = form;

        initListeners();
    }

    @Override
    /**
     * Initializes variables.
     */
    protected void init() {
        super.init();

        addButton = new Button("New "
                + StringUtils.toTitleCase(type.getSimpleName()));
        deleteButton = new Button("Delete "
                + StringUtils.toTitleCase(type.getSimpleName()));
    }

    @Override
    /**
     * Initializes layout.
     */
    protected void initLayout() {
        super.initLayout();

        ButtonSet buttons = new ButtonSet(addButton, deleteButton);
        layout.addComponents(buttons);
    }

    /**
     * Initializes listeners.
     */
    protected void initListeners() {
        // These first five listeners are used to disable selection in the list
        // when
        // the form is modified and to ask whether to discard changes when they
        // try
        // to change current item.
        form.addModifiedListener(new ModifiedListener() {
            public void modified(Field<?> field) {
                list.setSelectable(false);
            }
        });

        form.addCommitListener(new CommitListener() {
            public void commit(Object itemId) {
                container.refreshItem(itemId);
                list.setSelectable(true);
            }
        });

        form.addRevertListener(new RevertListener() {
            public void revert(Object itemId) {
                container.refreshItem(itemId);
                list.setSelectable(true);
            }
        });

        list.addItemClickListener(new ItemClickListener() {
            public void itemClick(final ItemClickEvent event) {
                if (form.isModified()
                        && !event.getItemId().equals(list.getValue())) {
                    confirmDeselect(new ConfirmDialog.Listener() {
                        public void onClose(ConfirmDialog dialog) {
                            if (dialog.isConfirmed()) {
                                form.revert();
                                list.setSelectable(true);
                                list.select(event.getItemId());
                            }
                        }
                    });
                }
            }
        });

        list.addValueChangeListener(new ValueChangeListener() {
            public void valueChange(ValueChangeEvent event) {
                Object id = list.getValue();
                if (id != null) {
                    form.setItem(list.getItem(id));
                    if (!deleteButton.isEnabled()) {
                        deleteButton.setEnabled(true);
                    }
                } else {
                    form.setItem(form.getEmpty());
                    if (deleteButton.isEnabled()) {
                        deleteButton.setEnabled(false);
                    }
                }
            }
        });

        // Standard list buttons
        addButton.addClickListener(new ClickListener() {
            public void buttonClick(ClickEvent event) {
                if (form.isModified()) {
                    confirmDeselect(new ConfirmDialog.Listener() {
                        public void onClose(ConfirmDialog dialog) {
                            if (dialog.isConfirmed()) {
                                form.revert();
                                addItem();
                            }
                        }
                    });
                } else {
                    addItem();
                }
            }
        });

        deleteButton.addClickListener(new ClickListener() {
            public void buttonClick(ClickEvent event) {
                final Object id = list.getValue();
                if (id != null) {
                    ConfirmDialog.show(
                            UI.getCurrent(),
                            "Confirm",
                            String.format(
                                    "Do you really want to delete the item with ID %s?",
                                    id.toString()), "Yes", "No",
                            new ConfirmDialog.Listener() {
                                public void onClose(ConfirmDialog dialog) {
                                    if (dialog.isConfirmed()) {
                                        boolean isLast = id.equals(container
                                                .lastItemId());
                                        try {
                                            container.removeItem(id);
                                            if (container.size() == 0) {
                                                list.select(null);
                                            } else if (isLast) {
                                                list.select(container
                                                        .lastItemId());
                                            } else {
                                                list.select(container
                                                        .nextItemId(id));
                                            }
                                        } catch (PersistenceException e) {
                                            Notification
                                                    .show("Unable to delete item! Most likely it's being used somewhere important.",
                                                            Type.ERROR_MESSAGE);
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            });
                }
            }
        });

        list.select(container.firstItemId());
    }

    /**
     * Internal method to add a new item to the list container.
     */
    private void addItem() {
        Object id;
        try {
            T obj = type.newInstance();
            for (AddListener listener : addListeners) {
                listener.beforeAdd(obj);
            }
            id = container.addEntity(obj);
            list.select(id);
        } catch (Exception e) {
            Notification.show("Unable to create item!", Type.ERROR_MESSAGE);
            e.printStackTrace();
        }
    }

    /**
     * Confirms user before deselecting a modified item.
     * 
     * @param listener
     *            accept listener
     */
    private void confirmDeselect(final ConfirmDialog.Listener listener) {
        ConfirmDialog
                .show(UI.getCurrent(),
                        "Confirm",
                        "You've made changes to the item. Are you sure you want to discard them?",
                        "Yes", "No", listener);
    }

    public void addItemAddListener(AddListener listener) {
        addListeners.add(listener);
    }

    public AbstractForm<T> getForm() {
        return form;
    }

}
