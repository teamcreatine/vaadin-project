package com.example.vaadinproject.forms.fields;

import com.example.vaadinproject.util.StringUtils;
import com.vaadin.ui.TextField;

/**
 * Simple class for text fields.
 * 
 * @author Jukka Viljala <jupevi@utu.fi>
 * 
 */
public class FormTextField extends TextField {

    /**
     * Creates a new text field. Infers property id from caption.
     * 
     * @param caption
     *            field caption
     * @param maxLength
     *            max length of field
     * @param fieldWidth
     *            field width
     */
    public FormTextField(String caption, int maxLength, int fieldWidth) {
        this(caption, StringUtils.toCamelCase(caption), maxLength, fieldWidth);
    }

    /**
     * Creates a new text field.
     * 
     * @param caption
     *            field caption
     * @param propertyId
     *            property id
     * @param maxLength
     *            max length of field
     * @param fieldWidth
     *            field width
     */
    public FormTextField(String caption, String propertyId, int maxLength,
            int fieldWidth) {
        this.setCaption(caption);
        this.setId(propertyId);
        this.setMaxLength(maxLength);
        if (fieldWidth > 0) {
            this.setWidth(fieldWidth, Unit.PIXELS);
        }
        this.setNullRepresentation("");

        this.setImmediate(true);
        this.setTextChangeEventMode(TextChangeEventMode.TIMEOUT);
        this.setTextChangeTimeout(50);
    }

}
