package com.example.vaadinproject.forms.fields;

import java.util.ArrayList;
import java.util.List;

import com.vaadin.ui.Table;

/**
 * Class for a table that fires refresh events.
 * 
 * @author Jukka Viljala <jupevi@utu.fi>
 * 
 */
public class FormTable extends Table {

    private final List<TableRefreshListener> refreshListeners;

    /**
     * Creates a new table.
     */
    public FormTable() {
        super();
        refreshListeners = new ArrayList<TableRefreshListener>();
    }

    @Override
    protected void refreshRenderedCells() {
        if (refreshListeners != null) {
            for (TableRefreshListener listener : refreshListeners) {
                listener.beforeRefresh();
            }
        }
        super.refreshRenderedCells();
    }

    public void addRefreshListener(TableRefreshListener listener) {
        refreshListeners.add(listener);
    }

    /**
     * Listener for refresh events.
     * 
     * @author Jukka Viljala <jupevi@utu.fi>
     * 
     */
    public interface TableRefreshListener {
        public void beforeRefresh();
    }

}
