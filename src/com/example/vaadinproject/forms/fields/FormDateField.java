package com.example.vaadinproject.forms.fields;

import javax.persistence.TemporalType;

import com.example.vaadinproject.util.StringUtils;
import com.vaadin.ui.DateField;

/**
 * Simple class for a date field.
 * 
 * @author Jukka Viljala <jupevi@utu.fi>
 * 
 */
public class FormDateField extends DateField {

    /**
     * Creates a new date field. Infers property id from caption.
     * 
     * @param caption
     *            field caption
     * @param fieldWidth
     *            field width
     * @param temporalType
     *            temporal type (not used yet)
     */
    public FormDateField(String caption, int fieldWidth,
            TemporalType temporalType) {
        this(caption, StringUtils.toCamelCase(caption), fieldWidth,
                temporalType);
    }

    /**
     * Creates a new date field.
     * 
     * @param caption
     *            field caption
     * @param propertyId
     *            property id
     * @param fieldWidth
     *            field width
     * @param temporalType
     *            temporal type (not used yet)
     */
    public FormDateField(String caption, String propertyId, int fieldWidth,
            TemporalType temporalType) {
        this.setCaption(caption);
        this.setId(propertyId);
        if (fieldWidth > 0) {
            this.setWidth(fieldWidth, Unit.PIXELS);
        }
        this.setImmediate(true);
    }

}
