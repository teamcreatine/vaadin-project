package com.example.vaadinproject.forms.fields;

import java.util.Collections;
import java.util.Set;

import org.vaadin.dialogs.ConfirmDialog;

import com.example.vaadinproject.entities.BaseEntity;
import com.example.vaadinproject.entities.HasTableRows;
import com.example.vaadinproject.entities.Product;
import com.example.vaadinproject.entities.TableRowEntity;
import com.example.vaadinproject.forms.fields.FormTable.TableRefreshListener;
import com.example.vaadinproject.forms.fields.FormTableFieldFactory.CellValueChangeEvent;
import com.example.vaadinproject.forms.fields.FormTableFieldFactory.CellValueChangeListener;
import com.example.vaadinproject.layouts.ButtonSet;
import com.example.vaadinproject.layouts.DefaultVerticalLayout;
import com.example.vaadinproject.util.JPAHelper;
import com.example.vaadinproject.util.StringUtils;
import com.example.vaadinproject.windows.ListSelectDialog;
import com.vaadin.addon.jpacontainer.EntityItem;
import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.data.util.filter.Compare;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Table.RowHeaderMode;

public class FormProductTable<E extends BaseEntity & HasTableRows, R extends TableRowEntity>
        extends CustomComponent {

    private final Class<R> rowType;
    private final JPAContainer<R> products;
    private EntityItem<E> currentItem;

    private final DefaultVerticalLayout layout;

    private final FormTable table;
    private final FormTableFieldFactory tableFactory;

    private ListSelectDialog<Product> productsDialog;
    private final Button addRowButton;
    private final Button addProductButton;
    private final Button removeRowsButton;

    private final ButtonSet tableButtons;

    private final String idColumn;

    public FormProductTable(Class<E> entityType, Class<R> rowType) {
        this.rowType = rowType;

        products = JPAHelper.getContainer(rowType);
        products.setApplyFiltersImmediately(false);
        layout = new DefaultVerticalLayout(false);

        table = new FormTable();
        tableFactory = new FormTableFieldFactory(rowType, table);
        addRowButton = new Button("Add Row");
        addProductButton = new Button("Add Product");
        removeRowsButton = new Button("Remove Selected Rows");

        tableButtons = new ButtonSet("", addRowButton, addProductButton);
        tableButtons.addComponent(removeRowsButton, "large-margin-left");

        idColumn = String.format("%s.id",
                StringUtils.toUncapitalized(entityType.getSimpleName()));

        initLayout();
        initListeners();
    }

    void initLayout() {
        table.setContainerDataSource(products);
        table.setPageLength(10);
        table.setCacheRate(0);
        table.setImmediate(true);
        table.setMultiSelect(true);
        table.setEditable(true);
        table.setSelectable(true);
        table.setSortEnabled(false);
        table.setRowHeaderMode(RowHeaderMode.INDEX);
        table.setTableFieldFactory(tableFactory);

        layout.addComponents(tableButtons, table);

        setCompositionRoot(layout);
    }

    void initListeners() {
        tableFactory.addShortcutListenerToTable();

        tableFactory.addCellValueChangeListener("code",
                new CellValueChangeListener() {
                    public void cellValueChange(CellValueChangeEvent event) {
                        if (event.isUserChanged()) {
                            if (tryAddProductByCode(event.getProperty()
                                    .getValue().toString(), event.getItem())) {
                                // For some reason, focus is lost after updating
                                // the item. Not an ideal solution but return
                                // focus to code column
                                event.getField().focus();
                            }
                        }
                    }
                });

        table.addRefreshListener(new TableRefreshListener() {
            public void beforeRefresh() {
                tableFactory.reset();
            }
        });

        addRowButton.addClickListener(new ClickListener() {
            public void buttonClick(ClickEvent event) {
                try {
                    R newEntity = rowType.newInstance();
                    newEntity.setParent(currentItem);
                    products.addEntity(newEntity);
                } catch (InstantiationException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        });

        addProductButton.addClickListener(new ClickListener() {
            public void buttonClick(ClickEvent event) {
                if (productsDialog == null) {
                    productsDialog = new ListSelectDialog<Product>(
                            Product.class, "Select Products to Add",
                            new Object[] { "id", "code", "name" });
                    productsDialog.setMultiSelect(true);
                    productsDialog.addAcceptListener(new ClickListener() {
                        public void buttonClick(ClickEvent event) {
                            for (Product product : productsDialog
                                    .getSelectedEntities()) {
                                R rowProduct = currentItem.getEntity()
                                        .productToRowProduct(product);
                                rowProduct.setParent(currentItem);
                                System.out.println(rowProduct);
                                products.addEntity(rowProduct);
                            }
                        }
                    });
                }
                productsDialog.show();
            }
        });

        removeRowsButton.addClickListener(new ClickListener() {
            public void buttonClick(ClickEvent event) {
                Object itemIdsObj = table.getValue();
                if (itemIdsObj != null && itemIdsObj instanceof Set) {
                    final Set<?> itemIds = (Set<?>) itemIdsObj;
                    if (itemIds.size() > 0) {
                        ConfirmDialog.show(getUI(), "Confirm", String.format(
                                "Do you really want to delete %d rows?",
                                itemIds.size()), "Yes", "No",
                                new ConfirmDialog.Listener() {
                                    public void onClose(ConfirmDialog dialog) {
                                        if (dialog.isConfirmed()) {
                                            for (Object itemId : itemIds) {
                                                products.removeItem(itemId);
                                            }
                                            // Selection isn't cleared
                                            // automatically
                                            table.setValue(Collections.EMPTY_SET);
                                        }
                                    }
                                });
                    }
                }
            }
        });
    }

    @SuppressWarnings("unchecked")
    /**
     * Tries to fill in row information from a product by code.
     * 
     * @param code code to find product for
     * @param item row item to modify
     * @return true if product was found
     */
    boolean tryAddProductByCode(String code, EntityItem<?> item) {
        Object[] properties = currentItem.getEntity().getPropertiesByCode(code);
        if (properties != null) {
            item.setBuffered(true);
            int i = 0;
            for (Object propertyId : table.getVisibleColumns()) {
                if (i == properties.length) {
                    break;
                }
                item.getItemProperty(propertyId).setValue(properties[i++]);
            }
            item.setBuffered(false);
            return true;
        }
        return false;
        // Product product = JPAHelper.query(Product.class, "code", code);
        // if (product != null) {
        // item.refresh();
        // item.setBuffered(true);
        // for (Object propertyId : table.getVisibleColumns()) {
        // item.getItemProperty(propertyId);
        // }
        //
        // item.getItemProperty("code").setValue(product.getCode());
        // item.getItemProperty("name").setValue(product.getName());
        // item.getItemProperty("price").setValue(
        // product.getPrice((Currency) currentItem.getItemProperty(
        // "currency").getValue()));
        // item.getItemProperty("qtyOrdered").setValue(1);
        // item.getItemProperty("qtyShipped").setValue(1);
        // item.getItemProperty("unit").setValue(product.getUnit());
        // item.setBuffered(false);
        // return true;
        // }
        // return false;
    }

    public void setParentItem(EntityItem<E> item) {
        currentItem = item;

        table.setValue(Collections.EMPTY_SET);
        tableFactory.reset();
        products.removeAllContainerFilters();
        products.addContainerFilter(new Compare.Equal(idColumn, item
                .getItemProperty("id").getValue()));
        products.applyFilters();
    }

    public void setVisibleColumns(Object... columns) {
        table.setVisibleColumns(columns);
    }

    public void setColumnHeaders(String... headers) {
        table.setColumnHeaders(headers);
    }

}
