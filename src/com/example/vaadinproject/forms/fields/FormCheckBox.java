package com.example.vaadinproject.forms.fields;

import com.example.vaadinproject.util.StringUtils;
import com.vaadin.ui.CheckBox;

/**
 * Simple class for a checkbox.
 * 
 * @author Jukka Viljala <jupevi@utu.fi>
 * 
 */
public class FormCheckBox extends CheckBox {

    /**
     * Creates a new checkbox. Infers property id from caption.
     * 
     * @param caption
     *            caption
     */
    public FormCheckBox(String caption) {
        this(caption, StringUtils.toCamelCase(caption));
    }

    /**
     * Creates a new checkbox.
     * 
     * @param caption
     *            caption
     * @param propertyId
     *            property id
     */
    public FormCheckBox(String caption, String propertyId) {
        this.setCaption(caption);
        this.setId(propertyId);
        this.setImmediate(true);
    }

}
