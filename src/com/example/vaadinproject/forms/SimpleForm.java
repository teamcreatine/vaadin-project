package com.example.vaadinproject.forms;

import com.example.vaadinproject.entities.BaseEntity;
import com.example.vaadinproject.layouts.DefaultVerticalLayout;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Panel;

/**
 * Simple form for entities. Only includes fields that have been properly
 * annotated in the entity.
 * 
 * @author Jukka Viljala <jupevi@utu.fi>
 * 
 * @param <T>
 *            type of the entity to edit
 */
public class SimpleForm<T extends BaseEntity> extends AbstractForm<T> {

    protected DefaultVerticalLayout layout;
    protected FormLayout form;

    /**
     * Creates a new simple editor for entity.
     * 
     * @param type
     *            type of entity to edit
     */
    public SimpleForm(Class<T> type) {
        super(type);

        init();
        initLayout();
        initBindings();
        initListeners();
    }

    /**
     * Initializes variables.
     */
    void init() {
        layout = new DefaultVerticalLayout();
        form = new FormLayout();
    }

    /**
     * Virtual method for subclasses to initializer listeners.
     */
    void initListeners() {

    }

    /**
     * Initializes layout.
     */
    void initLayout() {
        form.setMargin(false);
        form.addComponents(factory.getComponentCollection());
        layout.addComponents(form, buttons);
        setCompositionRoot(new Panel(factory.getFormTitle(), layout));
    }

}
