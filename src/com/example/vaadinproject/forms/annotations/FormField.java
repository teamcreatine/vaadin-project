package com.example.vaadinproject.forms.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.example.vaadinproject.util.StringUtils;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
/**
 * Interface for form input field annotations. Allows giving instructions to FormFactory using this annotation.
 * 
 * @author Jukka Viljala <jupevi@utu.fi>
 *
 */
public @interface FormField {

    public enum FieldType {
        UNDEF, STRING, INTEGER, DECIMAL, SELECT, TEMPORAL, BOOLEAN
    }

    /**
     * Input type of the field. Inferred if undefined.
     * 
     * @return type of field
     */
    public FieldType type() default FieldType.UNDEF;

    /**
     * Whether to show caption.
     * 
     * @return true to show caption
     */
    public boolean showCaption() default true;

    /**
     * Caption for input.
     * 
     * @return caption
     */
    public String caption() default "";

    /**
     * Width of field in pixels. Defaults to 0 (no width specified).
     * 
     * @return width
     */
    public int width() default 0;

    /**
     * Whether to automatically fill a select box using a JPAContainer.
     * 
     * @return true to autofill
     */
    public boolean autofill() default false;

    /**
     * Whether to allow null values in a select box.
     * 
     * @return true to allow null
     */
    public boolean allowNull() default true;

    /**
     * Caption property for select boxes.
     * 
     * @return caption property id
     */
    public String captionProperty() default "id";

    /**
     * Group name this input belongs in.
     * 
     * @return group name
     */
    public String group() default "ungrouped";

    /**
     * Property id of the field this field's visibility depends on. Useful to
     * hide certain fields when a checkbox is (un)checked.
     * 
     * @return field id
     */
    public String dependentField() default "";

    /**
     * Whether to bind this input in the FieldGroup of a form.
     * 
     * @return true to bind
     */
    public boolean bind() default true;

    /**
     * Whether this input fires modified events. When true, changing the value
     * enables save/revert buttons in an unbuffered form.
     * 
     * @return true to fire events
     */
    public boolean fireModifiedEvents() default true;

    /**
     * Class representing metadata for a form input field. Uses various
     * annotations on the field definition to fill in values.
     * 
     * @author Jukka Viljala <jupevi@utu.fi>
     * 
     */
    public class FormFieldMetadata {

        private final String propertyId;
        private String caption;
        private final int fieldWidth;
        private final boolean autofill;
        private boolean allowNull;
        private final String captionProperty;
        private final String group;
        private final String dependentField;
        private int length = 1000; // Default in case field has no column
                                   // annotation
        private final boolean bind;
        private final boolean fireModifiedEvents;
        private TemporalType temporalType;

        public FormFieldMetadata(java.lang.reflect.Field field) {
            FormField annotation = field.getAnnotation(FormField.class);
            propertyId = field.getName();
            if (annotation.showCaption()) {
                caption = annotation.caption();
                if ("".equals(caption)) {
                    caption = StringUtils.toTitleCase(field.getName());
                }
            }
            fieldWidth = annotation.width();
            autofill = annotation.autofill();
            allowNull = annotation.allowNull();
            captionProperty = annotation.captionProperty();
            group = annotation.group();
            dependentField = annotation.dependentField();
            bind = annotation.bind();
            fireModifiedEvents = annotation.fireModifiedEvents();
            if (field.isAnnotationPresent(Column.class)) {
                Column col = field.getAnnotation(Column.class);
                length = col.length();
                allowNull = col.nullable();
            } else if (field.isAnnotationPresent(JoinColumn.class)) {
                allowNull = field.getAnnotation(JoinColumn.class).nullable();
            }
            if (field.isAnnotationPresent(Temporal.class)) {
                temporalType = field.getAnnotation(Temporal.class).value();
            }
        }

        public String getPropertyId() {
            return propertyId;
        }

        public String getCaption() {
            return caption;
        }

        public int getFieldWidth() {
            return fieldWidth;
        }

        public int getLength() {
            return length;
        }

        public boolean shouldBind() {
            return bind;
        }

        public boolean doesFireModifiedEvents() {
            return fireModifiedEvents;
        }

        public String getGroup() {
            return group;
        }

        public boolean hasGroup() {
            return !group.equals("");
        }

        public String getDependentField() {
            return dependentField;
        }

        public boolean hasDependentField() {
            return !dependentField.equals("");
        }

        public TemporalType getTemporalType() {
            return temporalType;
        }

        public boolean isAutofill() {
            return autofill;
        }

        public String getCaptionProperty() {
            return captionProperty;
        }

        public boolean isAllowNull() {
            return allowNull;
        }

    }

}