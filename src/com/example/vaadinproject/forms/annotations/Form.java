package com.example.vaadinproject.forms.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
/**
 * Interface for form annotations. Kind of useless.
 * 
 * @author Jukka Viljala <jupevi@utu.fi>
 *
 */
public @interface Form {

    public String title() default "";

}
