package com.example.vaadinproject.forms;

import com.example.vaadinproject.entities.Currency;
import com.example.vaadinproject.entities.Customer;
import com.example.vaadinproject.entities.SalesOrder;
import com.example.vaadinproject.entities.SalesOrderProduct;
import com.example.vaadinproject.forms.fields.FormProductTable;
import com.example.vaadinproject.layouts.DefaultFormLayout;
import com.example.vaadinproject.layouts.DefaultHorizontalLayout;
import com.example.vaadinproject.layouts.DefaultVerticalLayout;
import com.example.vaadinproject.windows.ListSelectDialog;
import com.vaadin.addon.jpacontainer.EntityItem;
import com.vaadin.data.Item;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Panel;

/**
 * Complex form for editing sales order entities.
 * 
 * @author Jukka Viljala <jupevi@utu.fi>
 * 
 */
public class SalesOrderEditForm extends AbstractForm<SalesOrder> {

    private DefaultVerticalLayout layout;
    private DefaultHorizontalLayout infoLayout;
    private DefaultHorizontalLayout contactLayout;
    private DefaultFormLayout billingContactLayout;
    private DefaultFormLayout shippingContactLayout;

    private ListSelectDialog<Customer> customersDialog;
    private Button browseCustomersButton;

    private FormProductTable<SalesOrder, SalesOrderProduct> productTable;

    /**
     * Creates a new sales order entity editor form.
     */
    public SalesOrderEditForm() {
        super(SalesOrder.class, false);

        init();
        initLayout();
        initListeners();
        initBindings();
    }

    /**
     * Initializes variables.
     */
    void init() {
        layout = new DefaultVerticalLayout(false);
        infoLayout = new DefaultHorizontalLayout();
        contactLayout = new DefaultHorizontalLayout(false);
        shippingContactLayout = new DefaultFormLayout();
        billingContactLayout = new DefaultFormLayout();

        browseCustomersButton = new Button("Browse");

        productTable = new FormProductTable<SalesOrder, SalesOrderProduct>(
                SalesOrder.class, SalesOrderProduct.class);
    }

    /**
     * Initializes layout.
     */
    void initLayout() {
        infoLayout.addComponent(new DefaultVerticalLayout(false, factory
                .getField("customer"), browseCustomersButton));
        infoLayout.addComponents(factory.getComponentCollection("info"));
        layout.addComponents(new Panel("Basic Information", infoLayout));

        shippingContactLayout.addComponents(factory
                .getComponentCollection("shipping"));
        billingContactLayout.addComponents(factory
                .getComponentCollection("billing"));
        contactLayout.addComponents(new Panel("Shipping Contact",
                shippingContactLayout), new Panel("Billing Contact",
                billingContactLayout));

        productTable.setVisibleColumns("code", "name", "price", "qtyOrdered",
                "qtyShipped", "unit", "discount");
        productTable.setColumnHeaders("Code", "Name", "Price", "Ordered",
                "Shipped", "Unit", "Discount");

        layout.addComponents(contactLayout, productTable);

        setCompositionRoot(layout);
    }

    /**
     * Initializes listeners.
     */
    void initListeners() {
        browseCustomersButton.addClickListener(new ClickListener() {
            public void buttonClick(ClickEvent event) {
                if (customersDialog == null) {
                    customersDialog = new ListSelectDialog<Customer>(
                            Customer.class, "Select Customer", new Object[] {
                                    "id", "name" });
                    customersDialog.addAcceptListener(new ClickListener() {
                        public void buttonClick(ClickEvent event) {
                            setCustomer(customersDialog.getSelectedEntity());
                        }
                    });
                }
                customersDialog.show();
            }
        });
    }

    @SuppressWarnings("unchecked")
    /**
     * Sets the customer of the current entity.
     * 
     * @param customer customer
     */
    void setCustomer(Customer customer) {
        EntityItem<SalesOrder> item = getItem();
        item.setBuffered(true);
        item.getItemProperty("customer").setValue(customer);
        if (customer != null) {
            item.getItemProperty("billingName").setValue(customer.getName());
            item.getItemProperty("billingAddress").setValue(
                    customer.getAddress());
            item.getItemProperty("billingZipCode").setValue(
                    customer.getZipCode());
            item.getItemProperty("billingCity").setValue(customer.getCity());
            boolean shippingNotBilling = customer.getShippingNotBilling();
            item.getItemProperty("shippingName").setValue(
                    (shippingNotBilling ? customer.getShippingName() : customer
                            .getName()));
            item.getItemProperty("shippingAddress").setValue(
                    (shippingNotBilling ? customer.getShippingAddress()
                            : customer.getAddress()));
            item.getItemProperty("shippingZipCode").setValue(
                    (shippingNotBilling ? customer.getShippingZipCode()
                            : customer.getZipCode()));
            item.getItemProperty("shippingCity").setValue(
                    (shippingNotBilling ? customer.getShippingCity() : customer
                            .getCity()));
        }
        item.setBuffered(false);
        // TODO: Seems to require refreshing or next customer change throws a
        // merge exception. Cascade problem?
        item.refresh();
    }

    /**
     * Gets the currency used in this sales order.
     * 
     * @return currency
     */
    public Currency getCurrency() {
        return getEntity().getCurrency();
    }

    @SuppressWarnings("unchecked")
    @Override
    /**
     * Resets table factory and sets product row filters.
     */
    protected void onSetItem(Item item) {
        productTable.setParentItem((EntityItem<SalesOrder>) item);
    }

}
