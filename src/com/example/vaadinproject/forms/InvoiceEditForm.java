package com.example.vaadinproject.forms;

import java.util.GregorianCalendar;

import com.example.vaadinproject.entities.Currency;
import com.example.vaadinproject.entities.Customer;
import com.example.vaadinproject.entities.Invoice;
import com.example.vaadinproject.entities.InvoiceProduct;
import com.example.vaadinproject.entities.PaymentTerm;
import com.example.vaadinproject.entities.Product;
import com.example.vaadinproject.forms.fields.FormDateField;
import com.example.vaadinproject.forms.fields.FormProductTable;
import com.example.vaadinproject.layouts.DefaultFormLayout;
import com.example.vaadinproject.layouts.DefaultHorizontalLayout;
import com.example.vaadinproject.layouts.DefaultVerticalLayout;
import com.example.vaadinproject.util.JPAHelper;
import com.example.vaadinproject.windows.ListSelectDialog;
import com.ibm.icu.util.Calendar;
import com.vaadin.addon.jpacontainer.EntityItem;
import com.vaadin.data.Item;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Panel;

/**
 * Complex form for editing invoice entities.
 * 
 * @author Jukka Viljala <jupevi@utu.fi>
 * 
 */
public class InvoiceEditForm extends AbstractForm<Invoice> {

    private DefaultVerticalLayout layout;
    private DefaultHorizontalLayout upperHorizontal;
    private FormLayout contactLayout;
    private FormLayout infoLayout;

    private ListSelectDialog<Customer> customersDialog;
    private Button browseCustomersButton;

    private FormProductTable<Invoice, InvoiceProduct> productTable;

    /**
     * Creates a new invoice entity editor form.
     */
    public InvoiceEditForm() {
        super(Invoice.class, false);

        init();
        initLayout();
        initListeners();
        initBindings();
    }

    /**
     * Initializes variables.
     */
    void init() {
        layout = new DefaultVerticalLayout(false);
        upperHorizontal = new DefaultHorizontalLayout(false);
        contactLayout = new DefaultFormLayout();
        infoLayout = new DefaultFormLayout();

        browseCustomersButton = new Button("Browse");

        productTable = new FormProductTable<Invoice, InvoiceProduct>(
                Invoice.class, InvoiceProduct.class);
    }

    /**
     * Initializes layout.
     */
    void initLayout() {
        DefaultHorizontalLayout custBrowser = new DefaultHorizontalLayout(
                false, factory.getField("customer"), browseCustomersButton);
        custBrowser.setCaption("Customer");
        contactLayout.addComponent(custBrowser);

        contactLayout.addComponents(factory.getComponentCollection("contact"));
        infoLayout.addComponents(factory.getComponentCollection("info"));

        upperHorizontal.addComponent(new Panel("Contact", contactLayout));
        upperHorizontal.addComponent(new Panel("Information", infoLayout));

        productTable.setVisibleColumns("code", "name", "price", "taxClass",
                "quantity", "unit", "discount");
        productTable.setColumnHeaders("Code", "Name", "Price", "Tax Class",
                "Quantity", "Unit", "Discount");

        layout.addComponents(upperHorizontal, productTable);

        setCompositionRoot(layout);
    }

    /**
     * Initializes listeners.
     */
    void initListeners() {
        browseCustomersButton.addClickListener(new ClickListener() {
            public void buttonClick(ClickEvent event) {
                if (customersDialog == null) {
                    customersDialog = new ListSelectDialog<Customer>(
                            Customer.class, "Select Customer", new Object[] {
                                    "id", "name" });
                    customersDialog.addAcceptListener(new ClickListener() {
                        public void buttonClick(ClickEvent event) {
                            setCustomer(customersDialog.getSelectedEntity());
                        }
                    });
                }
                customersDialog.show();
            }
        });

        factory.getField("paymentTerm").addValueChangeListener(
                new ValueChangeListener() {
                    public void valueChange(ValueChangeEvent event) {
                        if (!isIgnoreValueChange()) {
                            setDueDate(event.getProperty().getValue());
                        }
                    }
                });
    }

    @SuppressWarnings("unchecked")
    /**
     * Sets the customer of the current entity.
     * 
     * @param customer customer
     */
    void setCustomer(Customer customer) {
        EntityItem<Invoice> item = getItem();
        item.setBuffered(true);
        item.getItemProperty("customer").setValue(customer);
        if (customer != null) {
            item.getItemProperty("name").setValue(customer.getName());
            item.getItemProperty("address").setValue(customer.getAddress());
            item.getItemProperty("zipCode").setValue(customer.getZipCode());
            item.getItemProperty("city").setValue(customer.getCity());
        }
        item.setBuffered(false);
        // seems to require refreshing or next customer change throws a merge
        // exception
        item.refresh();
    }

    @SuppressWarnings("unchecked")
    /**
     * Tries to fill in row information from a product by code.
     * 
     * @param code code to find product for
     * @param item row item to modify
     * @return true if product was found
     */
    boolean tryAddProductByCode(String code, EntityItem<?> item) {
        Product product = JPAHelper.query(Product.class, "code", code);
        if (product != null) {
            // Don't even remember why this was here
            // item.refresh();
            item.setBuffered(true);
            item.getItemProperty("code").setValue(product.getCode());
            item.getItemProperty("name").setValue(product.getName());
            item.getItemProperty("price").setValue(
                    product.getPrice(getCurrency()));
            item.getItemProperty("taxClass").setValue(product.getTaxClass());
            item.getItemProperty("quantity").setValue(1);
            item.getItemProperty("unit").setValue(product.getUnit());
            item.setBuffered(false);
            return true;
        }
        return false;
    }

    /**
     * Sets the due date after changing payment term.
     * 
     * @param itemId
     *            id of payment term
     */
    void setDueDate(Object itemId) {
        PaymentTerm term = JPAHelper.getEntity(PaymentTerm.class, itemId);
        if (term != null) {
            FormDateField dateField = (FormDateField) factory.getField("date");
            FormDateField dueDateField = (FormDateField) factory
                    .getField("dueDate");

            // GregCal should handle DST's
            GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTime(dateField.getValue());
            calendar.add(Calendar.DATE, term.getDays());
            dueDateField.setValue(calendar.getTime());
        }
    }

    /**
     * Gets the currency used in this invoice.
     * 
     * @return currency
     */
    public Currency getCurrency() {
        return getEntity().getCurrency();
    }

    @SuppressWarnings("unchecked")
    @Override
    /**
     * Resets table factory and sets product row filters.
     */
    protected void onSetItem(Item item) {
        productTable.setParentItem((EntityItem<Invoice>) item);
    }

}
