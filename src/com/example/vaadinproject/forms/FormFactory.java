package com.example.vaadinproject.forms;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.example.vaadinproject.forms.annotations.Form;
import com.example.vaadinproject.forms.annotations.FormField;
import com.example.vaadinproject.forms.annotations.FormField.FieldType;
import com.example.vaadinproject.forms.annotations.FormField.FormFieldMetadata;
import com.example.vaadinproject.forms.fields.FormCheckBox;
import com.example.vaadinproject.forms.fields.FormDateField;
import com.example.vaadinproject.forms.fields.FormDecimalField;
import com.example.vaadinproject.forms.fields.FormIntegerField;
import com.example.vaadinproject.forms.fields.FormNativeSelect;
import com.example.vaadinproject.forms.fields.FormTextField;
import com.example.vaadinproject.util.JPAHelper;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.Field;

/**
 * Factory class for forms. Use to automatically create fields based on entity
 * annotations.
 * 
 * @author Jukka Viljala <jupevi@utu.fi>
 * 
 */
public class FormFactory {

    /**
     * Type of entity the form edits.
     */
    private final Class<?> type;
    private final Map<String, Field<?>> fields;
    private final Map<String, List<Field<?>>> groups;
    private final Map<String, FormFieldMetadata> fieldMetadata;
    private final Map<String, List<Field<?>>> dependents;

    /**
     * Creates a new factory to get fields for an entity.
     * 
     * @param type
     *            entity type
     */
    public FormFactory(Class<?> type) {
        this.type = type;
        this.fields = new LinkedHashMap<String, Field<?>>();
        this.groups = new HashMap<String, List<Field<?>>>();
        this.fieldMetadata = new HashMap<String, FormFieldMetadata>();
        this.dependents = new HashMap<String, List<Field<?>>>();
    }

    /**
     * Gets the form title, if present.
     * 
     * @return form title or empty string
     */
    public String getFormTitle() {
        if (type.isAnnotationPresent(Form.class)) {
            return type.getAnnotation(Form.class).title();
        }
        return "";
    }

    /**
     * Creates all annotated fields.
     */
    public void createFields() {
        for (java.lang.reflect.Field field : type.getDeclaredFields()) {
            if (field.isAnnotationPresent(FormField.class)) {
                createField(field, field.getName(), false);
            }
        }
    }

    /**
     * Creates field for annotated property by id. Doesn't ignore field cache.
     * 
     * @param propertyId
     *            property id to create field for
     * @return Field instance or null if property isn't annotated properly
     */
    public Field<?> createField(String propertyId) {
        return createField(getDeclaredField(propertyId), propertyId, false);
    }

    /**
     * Creates field for annotated property by id, bypassing cache if specified.
     * 
     * @param propertyId
     *            property id to create field for
     * @param ignoreCache
     *            whether to bypass cache
     * @return Field instance or null if property isn't annotated properly
     */
    public Field<?> createField(String propertyId, boolean ignoreCache) {
        return createField(getDeclaredField(propertyId), propertyId,
                ignoreCache);
    }

    /**
     * Creates field for annotated property, bypassing cache if specified.
     * 
     * @param field
     *            property to create field for
     * @param propertyId
     *            property id
     * @param ignoreCache
     *            whether to bypass cache
     * @return
     */
    public Field<?> createField(java.lang.reflect.Field field,
            String propertyId, boolean ignoreCache) {
        if (field == null) {
            return null;
        }
        Field<?> formField = null;
        if (!ignoreCache) {
            // Return from cache if we've already created the field
            formField = getField(propertyId);
            if (formField != null) {
                return formField;
            }
        }
        // Create input field based on annotated information
        FormFieldMetadata metadata = new FormFieldMetadata(field);
        formField = internalCreateField(field, metadata);
        if (!ignoreCache && formField != null) {
            fieldMetadata.put(propertyId, metadata);
            fields.put(propertyId, formField);
            if (metadata.hasGroup()) {
                addFieldToGroup(metadata.getGroup(), formField);
            }
            if (metadata.hasDependentField()) {
                addFieldToDependents(metadata.getDependentField(), formField);
                toggleFieldVisibility(formField, metadata.getDependentField());
            }
        }
        return formField;
    }

    /**
     * Gets property for property id.
     * 
     * @param propertyId
     *            property id
     * @return property or null if not annotated
     */
    private java.lang.reflect.Field getDeclaredField(String propertyId) {
        try {
            java.lang.reflect.Field field = type.getDeclaredField(propertyId);
            if (field.isAnnotationPresent(FormField.class)) {
                return field;
            }
        } catch (NoSuchFieldException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SecurityException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Internally creates the input field for a property.
     * 
     * @param field
     *            property to create field for
     * @param metadata
     *            annotated data
     * @return
     */
    private Field<?> internalCreateField(java.lang.reflect.Field field,
            FormFieldMetadata metadata) {
        Field<?> formField = null;
        FieldType type = getInputFieldType(field);

        if (type == FieldType.STRING) {
            formField = new FormTextField(metadata.getCaption(),
                    metadata.getPropertyId(), metadata.getLength(),
                    metadata.getFieldWidth());
        } else if (type == FieldType.INTEGER) {
            formField = new FormIntegerField(metadata.getCaption(),
                    metadata.getPropertyId(), metadata.getFieldWidth());
        } else if (type == FieldType.DECIMAL) {
            formField = new FormDecimalField(metadata.getCaption(),
                    metadata.getPropertyId(), metadata.getFieldWidth());
        } else if (type == FieldType.SELECT) {
            formField = new FormNativeSelect(metadata.getCaption(),
                    metadata.getPropertyId(), metadata.getFieldWidth());
            if (metadata.isAutofill()) {
                FormNativeSelect select = (FormNativeSelect) formField;
                select.setContainerDataSource(JPAHelper.getContainer(field
                        .getType()));
                select.setNullSelectionAllowed(metadata.isAllowNull());
                select.setItemCaptionPropertyId(metadata.getCaptionProperty());
            }
        } else if (type == FieldType.TEMPORAL) {
            formField = new FormDateField(metadata.getCaption(),
                    metadata.getPropertyId(), metadata.getFieldWidth(),
                    metadata.getTemporalType());
        } else if (type == FieldType.BOOLEAN) {
            formField = new FormCheckBox(metadata.getCaption(),
                    metadata.getPropertyId());
            addVisibilityListener(formField);
        }

        return formField;
    }

    /**
     * Adds a listener that changes other field's visibility based on value
     * changes of this field.
     * 
     * @param field
     *            field to add listener to
     */
    private void addVisibilityListener(Field<?> field) {
        final String fieldId = field.getId();
        field.addValueChangeListener(new ValueChangeListener() {
            public void valueChange(ValueChangeEvent event) {
                // Value changed, get dependent fields and show/hide them
                boolean visible = (Boolean) event.getProperty().getValue();
                List<Field<?>> deps = dependents.get(fieldId);
                if (deps != null && deps.size() > 0) {
                    for (Field<?> dep : deps) {
                        toggleFieldVisibility(dep, visible);
                    }
                }
            }
        });
    }

    /**
     * Toggles dependent field visibility based on field id they are dependent
     * of.
     * 
     * @param field
     *            field to toggle visibility for
     * @param dependsOn
     *            id of field dependent of
     */
    private void toggleFieldVisibility(Field<?> field, String dependsOn) {
        this.toggleFieldVisibility(field, (Boolean) fields.get(dependsOn)
                .getValue());
    }

    /**
     * Toggles field visibility.
     * 
     * @param field
     *            field to toggle visibility for
     * @param visible
     *            whether field should be visible
     */
    private void toggleFieldVisibility(Field<?> field, boolean visible) {
        field.setVisible(visible);
    }

    /**
     * Adds field to group map so it can be fetched using the group name.
     * 
     * @param group
     *            group name
     * @param field
     *            field to add
     */
    private void addFieldToGroup(String group, Field<?> field) {
        List<Field<?>> list = groups.get(group);
        if (list == null) {
            list = new ArrayList<Field<?>>();
            groups.put(group, list);
        }
        list.add(field);
    }

    /**
     * Adds field to be dependent of another field.
     * 
     * @param dependsOn
     *            field id to depend on
     * @param field
     *            field that depends
     */
    private void addFieldToDependents(String dependsOn, Field<?> field) {
        List<Field<?>> list = dependents.get(dependsOn);
        if (list == null) {
            list = new ArrayList<Field<?>>();
            dependents.put(dependsOn, list);
        }
        list.add(field);
    }

    /**
     * Gets the input field type for a property. Tries to infer type if not
     * specified by annotation.
     * 
     * @param field
     *            field to get type for
     * @return input field type or null if not available
     */
    private FieldType getInputFieldType(java.lang.reflect.Field field) {
        FormField metadata = field.getAnnotation(FormField.class);
        if (metadata.type() != FieldType.UNDEF) {
            return metadata.type();
        }

        Type t = field.getType();
        if (t == String.class) {
            return FieldType.STRING;
        } else if (t == Integer.class) {
            return FieldType.INTEGER;
        } else if (t == BigDecimal.class) {
            return FieldType.DECIMAL;
        } else if (t == Date.class) {
            return FieldType.TEMPORAL;
        } else if (t == Boolean.class) {
            return FieldType.BOOLEAN;
        }
        return null;
    }

    /**
     * Gets annotated data for property id.
     * 
     * @param propertyId
     *            property id to get data for
     * @return annotated data
     */
    public FormFieldMetadata getMetadata(String propertyId) {
        return fieldMetadata.get(propertyId);
    }

    /**
     * Gets all created fields.
     * 
     * @return created fields
     */
    public Collection<Field<?>> getFields() {
        return fields.values();
    }

    /**
     * Gets all created fields belonging to a specified group.
     * 
     * @param group
     *            field group
     * @return created fields in group
     */
    public Collection<Field<?>> getFields(String group) {
        return groups.get(group);
    }

    /**
     * Gets single field using property id.
     * 
     * @param key
     *            property id
     * @return field
     */
    public Field<?> getField(String key) {
        return fields.get(key);
    }

    /**
     * Gets all created fields in a Component array to easily add to layouts.
     * 
     * @return Component array containing all fields
     */
    public Component[] getComponentCollection() {
        Collection<?> fields = getFields();
        if (fields != null) {
            return fields.toArray(new Component[fields.size()]);
        }
        return new Component[0];
    }

    /**
     * Gets created fields from a group in a Component array to easily add to
     * layouts.
     * 
     * @param group
     *            field group
     * @return Component array containing all fields from group
     */
    public Component[] getComponentCollection(String group) {
        Collection<?> fields = getFields(group);
        if (fields != null) {
            return fields.toArray(new Component[fields.size()]);
        }
        return new Component[0];
    }

}
