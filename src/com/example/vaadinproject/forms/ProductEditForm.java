package com.example.vaadinproject.forms;

import java.math.BigDecimal;
import java.util.Set;

import com.example.vaadinproject.entities.Currency;
import com.example.vaadinproject.entities.Product;
import com.example.vaadinproject.entities.ProductPrice;
import com.example.vaadinproject.entities.ProductSupplier;
import com.example.vaadinproject.entities.Supplier;
import com.example.vaadinproject.forms.fields.FormDecimalField;
import com.example.vaadinproject.forms.fields.FormNativeSelect;
import com.example.vaadinproject.layouts.ButtonSet;
import com.example.vaadinproject.util.JPAHelper;
import com.example.vaadinproject.util.JPAHelper.ContainerType;
import com.example.vaadinproject.windows.AddSuppliersDialog;
import com.vaadin.addon.jpacontainer.EntityItem;
import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.addon.jpacontainer.JPAContainerItem;
import com.vaadin.data.Container.ItemSetChangeEvent;
import com.vaadin.data.Container.ItemSetChangeListener;
import com.vaadin.data.Item;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.filter.Compare;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.UI;

/**
 * Complex form for editing product entities.
 * 
 * @author Jukka Viljala <jupevi@utu.fi>
 * 
 */
public class ProductEditForm extends SimpleForm<Product> {

    private JPAContainer<ProductSupplier> suppliers;
    private JPAContainer<ProductPrice> prices;

    private FormNativeSelect supplierSelect;
    private FormNativeSelect currencySelect;

    private FormDecimalField supplierPriceField;
    private FormDecimalField priceField;

    private Button addSuppliersButton;
    private Button removeSupplierButton;
    private ButtonSet supplierButtons;

    /**
     * Creates a new product entity editor form.
     */
    public ProductEditForm() {
        super(Product.class);
    }

    @Override
    /**
     * Initializes variables.
     */
    void init() {
        super.init();

        suppliers = JPAHelper.getContainer(ProductSupplier.class,
                ContainerType.BATCHABLE);
        suppliers.addNestedContainerProperty("supplier.name");
        suppliers.setApplyFiltersImmediately(false);

        prices = JPAHelper.getContainer(ProductPrice.class,
                ContainerType.BATCHABLE);
        prices.setApplyFiltersImmediately(false);

        supplierSelect = (FormNativeSelect) factory.getField("suppliers");
        supplierSelect.setNullSelectionAllowed(false);
        supplierSelect.setItemCaptionPropertyId("supplier.name");
        supplierSelect.setContainerDataSource(suppliers);
        supplierSelect.select(suppliers.firstItemId());

        currencySelect = (FormNativeSelect) factory.getField("currency");
        currencySelect.select(JPAHelper.getFirstItemId(Currency.class));

        supplierPriceField = (FormDecimalField) factory
                .getField("supplierPrice");
        priceField = (FormDecimalField) factory.getField("price");

        addSuppliersButton = new Button("Add Suppliers");
        removeSupplierButton = new Button("Remove Supplier");
        supplierButtons = new ButtonSet(addSuppliersButton,
                removeSupplierButton);
    }

    @Override
    /**
     * Adds the supplier buttons.
     */
    void initLayout() {
        super.initLayout();

        int index = form.getComponentIndex(supplierSelect);
        form.addComponent(supplierButtons, index + 1);
    }

    @Override
    /**
     * Initializes listeners.
     */
    void initListeners() {
        suppliers.addItemSetChangeListener(new ItemSetChangeListener() {
            public void containerItemSetChange(ItemSetChangeEvent event) {
                if (!isIgnoreValueChange()) {
                    modified(supplierSelect);
                }
            }
        });

        supplierSelect.addValueChangeListener(new ValueChangeListener() {
            public void valueChange(ValueChangeEvent event) {
                updateSupplierPrice();
                supplierPriceField.focus();
            }
        });

        supplierPriceField.addValueChangeListener(new ValueChangeListener() {
            @SuppressWarnings("unchecked")
            public void valueChange(ValueChangeEvent event) {
                if (isIgnoreValueChange()) {
                    return;
                }
                Object productSupplierId = supplierPriceField.getData();
                EntityItem<ProductSupplier> item = suppliers
                        .getItem(productSupplierId);
                if (item != null) {
                    BigDecimal value = supplierPriceField.getValue();
                    item.getItemProperty("price").setValue(value);
                }
            }
        });

        addSuppliersButton.addClickListener(new ClickListener() {
            public void buttonClick(ClickEvent event) {
                final AddSuppliersDialog dialog = new AddSuppliersDialog(
                        suppliers);
                dialog.addAcceptListener(new ClickListener() {
                    public void buttonClick(ClickEvent event) {
                        Set<Integer> ids = dialog.getSelectedIds();
                        if (ids.size() > 0) {
                            Object itemId = null;
                            for (final Integer id : ids) {
                                Supplier supplier = JPAHelper.getEntity(
                                        Supplier.class, id);
                                Product product = getEntity();
                                ProductSupplier prodsup = new ProductSupplier(
                                        supplier, product, null);
                                itemId = suppliers.addEntity(prodsup);
                            }
                            supplierSelect.select(itemId);
                        }
                    }
                });
                UI.getCurrent().addWindow(dialog);
            }
        });

        removeSupplierButton.addClickListener(new ClickListener() {
            public void buttonClick(ClickEvent event) {
                Object supplierId = supplierSelect.getValue();
                Object nextSupplier = suppliers.nextItemId(supplierId);
                suppliers.removeItem(supplierId);
                if (nextSupplier != null) {
                    supplierSelect.setValue(nextSupplier);
                } else if (suppliers.size() > 0) {
                    supplierSelect.setValue(suppliers.lastItemId());
                } else {
                    updateSupplierPrice();
                }
            }
        });

        currencySelect.addValueChangeListener(new ValueChangeListener() {
            public void valueChange(ValueChangeEvent event) {
                updatePrice();
                priceField.focus();
            }
        });

        // Update price field changes to the corresponding product price entity
        priceField.addValueChangeListener(new ValueChangeListener() {
            @SuppressWarnings("unchecked")
            public void valueChange(ValueChangeEvent event) {
                if (isIgnoreValueChange()) {
                    return;
                }
                Object productPriceId = priceField.getData();
                Object currencyId = currencySelect.getValue();
                if (currencyId == null) {
                    return;
                }
                JPAContainerItem<Currency> currencyItem = (JPAContainerItem<Currency>) currencySelect
                        .getItem(currencyId);
                Currency currency = currencyItem.getEntity();
                if (productPriceId != null) {
                    BigDecimal amount = priceField.getValue();
                    if (amount != null) {
                        EntityItem<ProductPrice> item = prices
                                .getItem(productPriceId);
                        item.getItemProperty("currency").setValue(currency);
                        item.getItemProperty("amount").setValue(amount);
                    } else {
                        prices.removeItem(productPriceId);
                    }
                } else if (event.getProperty().getValue() != null) {
                    ProductPrice price = new ProductPrice(getEntity(),
                            currency, event.getProperty().getValue().toString());
                    prices.addEntity(price);
                }
            }
        });
    }

    /**
     * Updates supplier price to match currently selected supplier.
     */
    void updateSupplierPrice() {
        if (suppliers.size() == 0) {
            supplierPriceField.setData(null);
            supplierPriceField.setValue(BigDecimal.ZERO);
            supplierPriceField.setEnabled(false);
        } else {
            EntityItem<ProductSupplier> item = suppliers.getItem(supplierSelect
                    .getValue());
            BigDecimal price = item.getEntity().getPrice();
            supplierPriceField.setData(item.getItemId());
            supplierPriceField.setValue(price);
            supplierPriceField.setEnabled(true);
        }
    }

    /**
     * Updates price to match currently selected currency.
     */
    void updatePrice() {
        Object currencyId = currencySelect.getValue();
        if (currencyId == null) {
            priceField.setData(null);
            priceField.setValue((BigDecimal) null);
            priceField.setEnabled(false);
        } else {
            priceField.setEnabled(true);
            EntityItem<ProductPrice> item = getPriceByCurrency((Integer) currencyId);
            if (item == null) {
                priceField.setData(null);
                priceField.setValue((BigDecimal) null);
            } else {
                BigDecimal price = item.getEntity().getAmount();
                priceField.setData(item.getItemId());
                priceField.setValue(price);
            }
        }
    }

    /**
     * Gets ProductPrice instance by currency id, wrapped in an EntityItem.
     * 
     * @param currencyId
     *            currency id
     * @return price or null
     */
    EntityItem<ProductPrice> getPriceByCurrency(int currencyId) {
        if (prices.size() == 0) {
            return null;
        }
        for (Object itemId : prices.getItemIds()) {
            EntityItem<ProductPrice> item = prices.getItem(itemId);
            if (item.getEntity().getCurrency().getId() == currencyId) {
                return item;
            }
        }
        return null;
    }

    @Override
    /**
     * Sets container filters and updates prices.
     */
    protected void onSetItem(Item item) {
        suppliers.removeAllContainerFilters();
        suppliers.addContainerFilter(new Compare.Equal("product.id", item
                .getItemProperty("id").getValue()));
        suppliers.applyFilters();
        supplierSelect.select(suppliers.firstItemId());

        prices.removeAllContainerFilters();
        prices.addContainerFilter(new Compare.Equal("product.id", item
                .getItemProperty("id").getValue()));
        prices.applyFilters();

        updatePrice();
        updateSupplierPrice();
    }

    @Override
    protected void onCommit() {
        // TODO: Clears current selection if it hasn't been persisted yet
        suppliers.commit();
        supplierSelect.select(suppliers.firstItemId());
        prices.commit();
    }

    @Override
    protected void onRevert() {
        suppliers.discard();
        prices.discard();

        updatePrice();
        updateSupplierPrice();
    }

}
