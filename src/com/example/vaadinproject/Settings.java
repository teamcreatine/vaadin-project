package com.example.vaadinproject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

import com.vaadin.server.VaadinServlet;

/**
 * Static class handling settings persistence.
 * 
 * @author Jukka Viljala <jupevi@utu.fi>
 * 
 */
public class Settings {

    public static final String SETTINGS_FILE = "META-INF/settings.properties";

    /**
     * New settings should be added here.
     */
    public static final Setting[] SETTINGS = new Setting[] {
            new Setting("Database File (relative to app root)", "databaseUrl",
                    "META-INF/database"),
            new Setting("Database User", "databaseUser", ""),
            new Setting("Database Pass (STORED PLAINTEXT)", "databasePass", ""),
            new Setting("JPA Logging Level", "loggingLevel", "OFF") };

    private static Properties properties;

    /**
     * Initialize settings by reading the properties file, if it exists.
     */
    public static void init() {
        properties = new Properties();

        if (exists()) {
            InputStream input = null;

            try {
                input = new FileInputStream(getFullPath());
                properties.load(input);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } finally {
                if (input != null) {
                    try {
                        input.close();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    /**
     * Checks if settings file exists.
     * 
     * @return true if file exists
     */
    public static boolean exists() {
        File file = new File(getFullPath());
        return file.exists() && !file.isDirectory();
    }

    /**
     * Finds a Setting instance based on key.
     * 
     * @param key
     *            setting name
     * @return Setting instance or null if not found
     */
    private static Setting find(String key) {
        for (Setting setting : SETTINGS) {
            if (setting.getProperty().equals(key)) {
                return setting;
            }
        }
        return null;
    }

    /**
     * Gets the value associated with a setting key. Returns default value for
     * setting if it doesn't exist in our Properties instance.
     * 
     * @param key
     *            setting name
     * @return value of the setting currently stored in our Properties instance
     */
    public static String get(String key) {
        if (properties == null) {
            init();
        }
        String def = null;
        Setting setting = find(key);
        if (setting != null) {
            def = setting.getDefaultValue();
        }
        return properties.getProperty(key, def);
    }

    /**
     * Sets the value associated with a setting key. Does not automatically
     * persist settings.
     * 
     * @param key
     *            setting name
     * @param value
     *            new value
     * @return value
     */
    public static String set(String key, String value) {
        if (properties == null) {
            init();
        }
        properties.setProperty(key, value);
        return value;
    }

    /**
     * Persist settings in file.
     */
    public static void store() {
        OutputStream output = null;
        try {
            output = new FileOutputStream(getFullPath());
            properties.store(output, null);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Gets full path to settings file.
     * 
     * @return path to settings file
     */
    public static String getFullPath() {
        return getFullPath(SETTINGS_FILE);
    }

    /**
     * Gets full path from partial path (relative to application root).
     * 
     * @param partialPath
     *            path relative to app root
     * @return full path
     */
    public static String getFullPath(String partialPath) {
        return VaadinServlet.getCurrent().getServletContext()
                .getRealPath(partialPath);
    }

}
