package com.example.vaadinproject.entities;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.example.vaadinproject.forms.annotations.FormField;
import com.example.vaadinproject.forms.annotations.FormField.FieldType;
import com.example.vaadinproject.util.JPAHelper;

@Entity
/**
 * Invoice entity. Also specifies form fields for editor form using annotations.
 * 
 * @author Jukka Viljala <jupevi@utu.fi>
 *
 */
public class Invoice extends TimestampedEntity implements HasTableRows {

    @OneToOne
    private SalesOrder salesOrder;

    @OneToOne
    @FormField(width = 50, type = FieldType.INTEGER, showCaption = false)
    private Customer customer;

    @Column(length = 128)
    @FormField(width = 200, group = "contact")
    private String name;

    @Column(length = 256)
    @FormField(width = 250, group = "contact")
    private String address;

    @Column(length = 20)
    @FormField(width = 80, group = "contact")
    private String zipCode;

    @Column(length = 96)
    @FormField(width = 150, group = "contact")
    private String city;

    @JoinColumn(nullable = false)
    @FormField(type = FieldType.SELECT, autofill = true, captionProperty = "isoName", group = "info")
    private Currency currency;

    @Temporal(TemporalType.DATE)
    @FormField(group = "info")
    private Date date;

    @FormField(type = FieldType.SELECT, autofill = true, captionProperty = "name", group = "info")
    private PaymentTerm paymentTerm;

    @Temporal(TemporalType.DATE)
    @FormField(group = "info")
    private Date dueDate;

    @FormField(group = "info")
    private BigDecimal penalInterest;

    @Column(length = 52)
    @FormField(width = 100, group = "info")
    private String reference;

    @Column(length = 52)
    @FormField(width = 100, group = "info")
    private String customerReference;

    @OneToMany(mappedBy = "invoice", cascade = CascadeType.ALL)
    private List<InvoiceProduct> products;

    public Invoice() {
        date = new Date();
        dueDate = new Date();
        // Since currency isn't nullable we need a default value
        currency = JPAHelper.getFirstEntity(Currency.class);
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public SalesOrder getOrder() {
        return salesOrder;
    }

    public void setOrder(SalesOrder order) {
        this.salesOrder = order;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public PaymentTerm getPaymentTerm() {
        return paymentTerm;
    }

    public void setPaymentTerm(PaymentTerm paymentTerm) {
        this.paymentTerm = paymentTerm;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getCustomerReference() {
        return customerReference;
    }

    public void setCustomerReference(String customerReference) {
        this.customerReference = customerReference;
    }

    public List<InvoiceProduct> getProducts() {
        return products;
    }

    public void setProducts(List<InvoiceProduct> products) {
        this.products = products;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public SalesOrder getSalesOrder() {
        return salesOrder;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public BigDecimal getPenalInterest() {
        return penalInterest;
    }

    public void setPenalInterest(BigDecimal penalInterest) {
        this.penalInterest = penalInterest;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends TableRowEntity> T productToRowProduct(Product product) {
        InvoiceProduct ip = new InvoiceProduct();
        ip.setCode(product.getCode());
        ip.setName(product.getName());
        ip.setPrice(product.getPrice(currency));
        ip.setTaxClass(product.getTaxClass());
        ip.setQuantity(new BigDecimal("1"));
        ip.setUnit(product.getUnit());
        return (T) ip;
    }

    public Object[] getPropertiesByCode(String code) {
        Product product = JPAHelper.query(Product.class, "code", code);
        if (product != null) {
            return new Object[] { product.getCode(), product.getName(),
                    product.getPrice(currency), product.getTaxClass(), 1,
                    product.getUnit(), };
        }
        return null;
    }

    @Override
    public String toString() {
        return "Invoice [customer=" + customer + ", name=" + name + ", id="
                + id + "]";
    }

}
