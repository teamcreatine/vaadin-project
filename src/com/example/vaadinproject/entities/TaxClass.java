package com.example.vaadinproject.entities;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import com.example.vaadinproject.forms.annotations.Form;
import com.example.vaadinproject.forms.annotations.FormField;

@Entity
@Form(title = "Edit Tax Class")
/**
 * Tax class entity. Also specifies form fields for editor form using annotations.
 * 
 * @author Jukka Viljala <jupevi@utu.fi>
 *
 */
public class TaxClass extends BaseEntity {

    @OneToMany(mappedBy = "taxClass")
    private Set<Product> products;

    @Column(length = 24)
    @FormField(width = 100)
    private String name;

    @FormField(width = 50)
    private Integer value;

    public TaxClass() {

    }

    public TaxClass(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "TaxClass [name=" + name + ", value=" + value + ", id=" + id
                + "]";
    }

}
