package com.example.vaadinproject.entities;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import com.example.vaadinproject.forms.annotations.Form;
import com.example.vaadinproject.forms.annotations.FormField;
import com.example.vaadinproject.forms.annotations.FormField.FieldType;

@Entity
@Form(title = "Edit Product")
/**
 * Product entity. Also specifies form fields for editor form using annotations.
 * 
 * @author Jukka Viljala <jupevi@utu.fi>
 *
 */
public class Product extends TimestampedEntity {

    @Column(length = 52)
    @FormField(width = 100)
    private String code;

    @Column(length = 256)
    @FormField(width = 200)
    private String name;

    @Column(length = 13)
    @FormField(width = 130)
    private String barcode;

    @OneToOne
    @FormField(type = FieldType.SELECT, autofill = true, captionProperty = "name")
    private TaxClass taxClass;

    @FormField(width = 50)
    private Integer inStock;

    @Column(length = 15)
    @FormField(width = 80)
    private String unit;

    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL)
    @FormField(type = FieldType.SELECT, bind = false, fireModifiedEvents = false)
    private Set<ProductSupplier> suppliers = new LinkedHashSet<ProductSupplier>();

    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL)
    private final Set<ProductPrice> prices = new LinkedHashSet<ProductPrice>();

    @Transient
    @FormField(width = 80, bind = false)
    private BigDecimal supplierPrice;

    @Transient
    @FormField(type = FieldType.SELECT, bind = false, autofill = true, allowNull = false, captionProperty = "isoName", fireModifiedEvents = false)
    private Currency currency;

    @Transient
    @FormField(width = 80, bind = false)
    private BigDecimal price;

    private boolean enabled;

    public Product() {

    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public Set<ProductPrice> getPrices() {
        return this.prices;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public TaxClass getTaxClass() {
        return taxClass;
    }

    public void setTaxClass(TaxClass taxClass) {
        this.taxClass = taxClass;
    }

    public Set<ProductSupplier> getSuppliers() {
        return suppliers;
    }

    public void setSuppliers(Set<ProductSupplier> suppliers) {
        this.suppliers = suppliers;
    }

    public void addSupplier(ProductSupplier supplier) {
        this.suppliers.add(supplier);
    }

    public Integer getInStock() {
        return inStock;
    }

    public void setInStock(Integer inStock) {
        this.inStock = inStock;
    }

    /**
     * Gets price for specified currency.
     * 
     * @param currency
     *            currency
     * @return price for currency
     */
    public BigDecimal getPrice(Currency currency) {
        Iterator<ProductPrice> iter = prices.iterator();
        while (iter.hasNext()) {
            ProductPrice price = iter.next();
            if (price.getCurrency().equals(currency)) {
                return price.getAmount();
            }
        }
        return null;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    @Override
    public String toString() {
        return "Product [name=" + name + ", id=" + id + "]";
    }

}
