package com.example.vaadinproject.entities;

import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "CURRENCY_ID",
        "PRODUCT_ID" }))
/**
 * Product price entity. Each currency has its own price.
 * 
 * @author Jukka Viljala <jupevi@utu.fi>
 *
 */
public class ProductPrice extends BaseEntity {

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(nullable = false)
    private Product product;

    @JoinColumn(nullable = false)
    private Currency currency;

    private BigDecimal amount;

    public ProductPrice() {

    }

    public ProductPrice(Product product, Currency currency, String amount) {
        this(product, currency, new BigDecimal(amount));
    }

    public ProductPrice(Product product, Currency currency, BigDecimal amount) {
        this.product = product;
        this.currency = currency;
        this.amount = amount;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "ProductPrice [product=" + product + ", currency=" + currency
                + ", amount=" + amount + ", id=" + id + "]";
    }

}
