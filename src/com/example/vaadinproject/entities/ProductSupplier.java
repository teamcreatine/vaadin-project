package com.example.vaadinproject.entities;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "SUPPLIER_ID",
        "PRODUCT_ID" }))
/**
 * Product supplier entity. A product can have many suppliers and each supplier can have a different price.
 * 
 * @author Jukka Viljala <jupevi@utu.fi>
 *
 */
public class ProductSupplier extends BaseEntity {

    @ManyToOne
    @JoinColumn(nullable = false)
    private Supplier supplier;

    @ManyToOne
    @JoinColumn(nullable = false)
    private Product product;

    @Column(precision = 20, scale = 4)
    private BigDecimal price;

    public ProductSupplier() {

    }

    public ProductSupplier(Supplier supplier, Product product, BigDecimal price) {
        this.supplier = supplier;
        this.product = product;
        this.price = price;
    }

    public Supplier getSupplier() {
        return supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "ProductSupplier [supplier=" + supplier + ", product=" + product
                + ", price=" + price + ", id=" + id + "]";
    }

}
