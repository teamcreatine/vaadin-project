package com.example.vaadinproject.entities;

public interface HasTableRows {

    /**
     * Gets table row instance from product instance.
     * 
     * @param product
     *            product to convert
     * @return table row product
     */
    public <T extends TableRowEntity> T productToRowProduct(Product product);

    /**
     * Queries product entity by code, and returns select properties as an array
     * to be used in setting cell data in a table row.
     * 
     * @param code
     *            product code
     * @return array of properties, indexed
     */
    public Object[] getPropertiesByCode(String code);

}
