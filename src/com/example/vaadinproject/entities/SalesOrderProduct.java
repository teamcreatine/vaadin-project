package com.example.vaadinproject.entities;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.example.vaadinproject.forms.annotations.FormField;
import com.vaadin.addon.jpacontainer.EntityItem;

@Entity
/**
 * Sales order product entity. Also specifies form fields for editor table using annotations.
 * 
 * @author Jukka Viljala <jupevi@utu.fi>
 *
 */
public class SalesOrderProduct extends BaseEntity implements TableRowEntity {

    @ManyToOne
    @JoinColumn(nullable = false)
    private SalesOrder salesOrder;

    @Column(length = 52)
    @FormField(width = 100)
    private String code;

    @Column(length = 256)
    @FormField(width = 250)
    private String name;

    @FormField(width = 80)
    private BigDecimal price;

    @FormField(width = 80)
    private BigDecimal qtyOrdered;

    @FormField(width = 80)
    private BigDecimal qtyShipped;

    @Column(length = 6)
    @FormField(width = 60)
    private String unit;

    @FormField(width = 60)
    private BigDecimal discount;

    public SalesOrderProduct() {

    }

    public SalesOrderProduct(SalesOrder order) {
        this.salesOrder = order;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getQtyOrdered() {
        return qtyOrdered;
    }

    public void setQtyOrdered(BigDecimal qtyOrdered) {
        this.qtyOrdered = qtyOrdered;
    }

    public BigDecimal getQtyShipped() {
        return qtyShipped;
    }

    public void setQtyShipped(BigDecimal qtyShipped) {
        this.qtyShipped = qtyShipped;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public SalesOrder getSalesOrder() {
        return salesOrder;
    }

    public void setSalesOrder(SalesOrder salesOrder) {
        this.salesOrder = salesOrder;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    @Override
    public void setParent(EntityItem<? extends BaseEntity> item) {
        this.salesOrder = (SalesOrder) item.getEntity();
    }

    @Override
    public String toString() {
        return "SalesOrderProduct [salesOrder=" + salesOrder + ", name=" + name
                + ", id=" + id + "]";
    }

}
