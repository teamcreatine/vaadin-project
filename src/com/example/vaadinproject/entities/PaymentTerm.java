package com.example.vaadinproject.entities;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.example.vaadinproject.forms.annotations.Form;
import com.example.vaadinproject.forms.annotations.FormField;

@Entity
@Form(title = "Edit Payment Term")
/**
 * Payment term entity. Also specifies form fields for editor form using annotations.
 * 
 * @author Jukka Viljala <jupevi@utu.fi>
 *
 */
public class PaymentTerm extends BaseEntity {

    @Column(length = 52)
    @FormField(width = 150)
    private String name;

    @FormField(width = 60)
    private Integer days;

    public PaymentTerm() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getDays() {
        return days;
    }

    public void setDays(Integer days) {
        this.days = days;
    }

    @Override
    public String toString() {
        return "PaymentTerm [name=" + name + ", id=" + id + "]";
    }

}
