package com.example.vaadinproject.entities;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import com.example.vaadinproject.forms.annotations.Form;
import com.example.vaadinproject.forms.annotations.FormField;

@Entity
@Form(title = "Edit Customer")
/**
 * Customer entity. Also specifies form fields for editor form using annotations.
 * 
 * @author Jukka Viljala <jupevi@utu.fi>
 *
 */
public class Customer extends TimestampedEntity {

    @OneToMany(mappedBy = "customer")
    private Set<SalesOrder> salesOrders;

    @OneToMany(mappedBy = "customer")
    private Set<Invoice> invoices;

    @Column(length = 128)
    @FormField(width = 200)
    private String name;

    @Column(length = 256)
    @FormField(width = 250)
    private String address;

    @Column(length = 10)
    @FormField(width = 80)
    private String zipCode;

    @Column(length = 96)
    @FormField(width = 150)
    private String city;

    @Column(length = 20)
    @FormField(width = 100)
    private String phone;

    @Column(length = 150)
    @FormField(width = 150)
    private String email;

    @Column(length = 10)
    @FormField(caption = "VAT", width = 80)
    private String vat;

    @Column(nullable = false)
    @FormField(caption = "Different Address for Shipping")
    private Boolean shippingNotBilling = false;

    @Column(length = 128)
    @FormField(width = 200, dependentField = "shippingNotBilling")
    private String shippingName;

    @Column(length = 256)
    @FormField(width = 250, dependentField = "shippingNotBilling")
    private String shippingAddress;

    @Column(length = 10)
    @FormField(width = 80, dependentField = "shippingNotBilling")
    private String shippingZipCode;

    @Column(length = 96)
    @FormField(width = 150, dependentField = "shippingNotBilling")
    private String shippingCity;

    public Customer() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getVat() {
        return vat;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }

    public String getShippingName() {
        return shippingName;
    }

    public void setShippingName(String shippingName) {
        this.shippingName = shippingName;
    }

    public String getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(String shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public String getShippingZipCode() {
        return shippingZipCode;
    }

    public void setShippingZipCode(String shippingZipCode) {
        this.shippingZipCode = shippingZipCode;
    }

    public String getShippingCity() {
        return shippingCity;
    }

    public void setShippingCity(String shippingCity) {
        this.shippingCity = shippingCity;
    }

    public Set<SalesOrder> getSalesOrders() {
        return salesOrders;
    }

    public Set<Invoice> getInvoices() {
        return invoices;
    }

    public Boolean getShippingNotBilling() {
        return shippingNotBilling;
    }

    public void setShippingNotBilling(Boolean shippingNotBilling) {
        this.shippingNotBilling = shippingNotBilling;
    }

    @Override
    public String toString() {
        return "Customer [name=" + name + ", id=" + id + "]";
    }

}
