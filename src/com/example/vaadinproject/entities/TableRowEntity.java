package com.example.vaadinproject.entities;

import com.vaadin.addon.jpacontainer.EntityItem;

public interface TableRowEntity {

    public void setParent(EntityItem<? extends BaseEntity> item);

}
