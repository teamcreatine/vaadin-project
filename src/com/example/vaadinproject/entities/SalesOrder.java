package com.example.vaadinproject.entities;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.example.vaadinproject.forms.annotations.FormField;
import com.example.vaadinproject.forms.annotations.FormField.FieldType;
import com.example.vaadinproject.util.JPAHelper;

@Entity
/**
 * Sales order entity. Also specifies form fields for editor form using annotations.
 * 
 * @author Jukka Viljala <jupevi@utu.fi>
 *
 */
public class SalesOrder extends TimestampedEntity implements HasTableRows {

    @OneToOne
    private Invoice invoice;

    @OneToOne
    @FormField(width = 50, type = FieldType.INTEGER, caption = "Customer")
    private Customer customer;

    @Temporal(TemporalType.DATE)
    @FormField(group = "info")
    private Date date;

    @JoinColumn(nullable = false)
    @FormField(type = FieldType.SELECT, autofill = true, captionProperty = "isoName", group = "info")
    private Currency currency;

    @OneToMany(mappedBy = "salesOrder", cascade = CascadeType.ALL)
    private List<SalesOrderProduct> products;

    @Column(length = 128)
    @FormField(width = 200, group = "shipping")
    private String shippingName;

    @Column(length = 256)
    @FormField(width = 250, group = "shipping")
    private String shippingAddress;

    @Column(length = 20)
    @FormField(width = 80, group = "shipping")
    private String shippingZipCode;

    @Column(length = 96)
    @FormField(width = 150, group = "shipping")
    private String shippingCity;

    @Column(length = 128)
    @FormField(width = 200, group = "billing")
    private String billingName;

    @Column(length = 256)
    @FormField(width = 250, group = "billing")
    private String billingAddress;

    @Column(length = 20)
    @FormField(width = 80, group = "billing")
    private String billingZipCode;

    @Column(length = 96)
    @FormField(width = 150, group = "billing")
    private String billingCity;

    public SalesOrder() {
        date = new Date();
        // Currency isn't nullable, so get default value
        currency = JPAHelper.getFirstEntity(Currency.class);
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Invoice getInvoice() {
        return invoice;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }

    public String getShippingName() {
        return shippingName;
    }

    public void setShippingName(String shippingName) {
        this.shippingName = shippingName;
    }

    public String getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(String shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public String getShippingZipCode() {
        return shippingZipCode;
    }

    public void setShippingZipCode(String shippingZipCode) {
        this.shippingZipCode = shippingZipCode;
    }

    public String getShippingCity() {
        return shippingCity;
    }

    public void setShippingCity(String shippingCity) {
        this.shippingCity = shippingCity;
    }

    public String getBillingName() {
        return billingName;
    }

    public void setBillingName(String billingName) {
        this.billingName = billingName;
    }

    public String getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(String billingAddress) {
        this.billingAddress = billingAddress;
    }

    public String getBillingZipCode() {
        return billingZipCode;
    }

    public void setBillingZipCode(String billingZipCode) {
        this.billingZipCode = billingZipCode;
    }

    public String getBillingCity() {
        return billingCity;
    }

    public void setBillingCity(String billingCity) {
        this.billingCity = billingCity;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public List<SalesOrderProduct> getProducts() {
        return products;
    }

    public void setProducts(List<SalesOrderProduct> products) {
        this.products = products;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends TableRowEntity> T productToRowProduct(Product product) {
        SalesOrderProduct op = new SalesOrderProduct();
        op.setCode(product.getCode());
        op.setName(product.getName());
        op.setPrice(product.getPrice(currency));
        op.setQtyOrdered(new BigDecimal("1"));
        op.setQtyShipped(new BigDecimal("1"));
        op.setUnit(product.getUnit());

        return (T) op;
    }

    @Override
    public Object[] getPropertiesByCode(String code) {
        Product product = JPAHelper.query(Product.class, "code", code);
        if (product != null) {
            return new Object[] { product.getCode(), product.getName(),
                    product.getPrice(currency), 1, 1, product.getUnit(), };
        }
        return null;
    }

    @Override
    public String toString() {
        return "SalesOrder [invoice=" + invoice + ", customer=" + customer
                + ", shippingName=" + shippingName + ", id=" + id + "]";
    }

}
