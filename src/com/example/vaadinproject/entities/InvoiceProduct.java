package com.example.vaadinproject.entities;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.example.vaadinproject.forms.annotations.FormField;
import com.example.vaadinproject.forms.annotations.FormField.FieldType;
import com.vaadin.addon.jpacontainer.EntityItem;

@Entity
/**
 * Invoice product entity. Also specifies form fields for editor table using annotations.
 * 
 * @author Jukka Viljala <jupevi@utu.fi>
 *
 */
public class InvoiceProduct extends BaseEntity implements TableRowEntity {

    @ManyToOne
    private Invoice invoice;

    @Column(length = 52)
    @FormField(width = 100)
    private String code;

    @Column(length = 256)
    @FormField(width = 250)
    private String name;

    @Column(precision = 20, scale = 4)
    @FormField(width = 80)
    private BigDecimal price;

    @FormField(type = FieldType.SELECT, autofill = true, captionProperty = "name")
    private TaxClass taxClass;

    @Column(precision = 20, scale = 4)
    @FormField(width = 80)
    private BigDecimal quantity;

    @Column(length = 6)
    @FormField(width = 60)
    private String unit;

    @Column(precision = 6, scale = 2)
    @FormField(width = 60)
    private BigDecimal discount;

    public InvoiceProduct() {

    }

    public InvoiceProduct(Invoice invoice) {
        this.invoice = invoice;
    }

    public Invoice getInvoice() {
        return invoice;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public TaxClass getTaxClass() {
        return taxClass;
    }

    public void setTaxClass(TaxClass taxClass) {
        this.taxClass = taxClass;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    @Override
    public void setParent(EntityItem<? extends BaseEntity> item) {
        this.invoice = (Invoice) item.getEntity();
    }

    @Override
    public String toString() {
        return "InvoiceProduct [invoice=" + invoice + ", name=" + name
                + ", id=" + id + "]";
    }

}
