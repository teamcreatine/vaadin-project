package com.example.vaadinproject.entities;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.example.vaadinproject.forms.annotations.Form;
import com.example.vaadinproject.forms.annotations.FormField;

@Entity
@Form(title = "Edit Currency")
/**
 * Currency entity. Also specifies form fields for editor form using annotations.
 * 
 * @author Jukka Viljala <jupevi@utu.fi>
 *
 */
public class Currency extends BaseEntity {

    @Column(length = 52)
    @FormField(width = 150)
    private String name;

    @Column(length = 3)
    @FormField(width = 40, caption = "ISO Name")
    private String isoName;

    public Currency() {

    }

    public Currency(String name, String isoName) {
        this.name = name;
        this.isoName = isoName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIsoName() {
        return isoName;
    }

    public void setIsoName(String isoName) {
        this.isoName = isoName;
    }

    @Override
    public String toString() {
        return "Currency [name=" + name + ", isoName=" + isoName + "]";
    }

}
