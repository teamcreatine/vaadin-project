package com.example.vaadinproject.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import com.example.vaadinproject.forms.annotations.Form;
import com.example.vaadinproject.forms.annotations.FormField;

@Entity
@Form(title = "Edit Supplier")
/**
 * Supplier entity. Also specifies form fields for editor form using annotations.
 * 
 * @author Jukka Viljala <jupevi@utu.fi>
 *
 */
public class Supplier extends TimestampedEntity {

    @Column(length = 128)
    @FormField(width = 200)
    private String name;

    @Column(length = 256)
    @FormField(width = 250)
    private String address;

    @Column(length = 10)
    @FormField(width = 80)
    private String zipCode;

    @Column(length = 96)
    @FormField(width = 150)
    private String city;

    @Column(length = 20)
    @FormField(width = 100)
    private String phone;

    @Column(length = 128)
    @FormField(width = 150)
    private String email;

    @Column(length = 10)
    @FormField(caption = "VAT", width = 80)
    private String vat;

    // Removing product suppliers on cascade works but isn't refreshed in
    // product form. In any case it's probably best to not let users delete
    // suppliers that are used in products.
    // @OneToMany(mappedBy = "supplier", cascade = CascadeType.REMOVE)
    @OneToMany(mappedBy = "supplier")
    private Set<ProductSupplier> products = new HashSet<ProductSupplier>();

    public Supplier() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getVat() {
        return vat;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }

    public Set<ProductSupplier> getProducts() {
        return products;
    }

    public void setProducts(Set<ProductSupplier> products) {
        this.products = products;
    }

    @Override
    public String toString() {
        return "Supplier [name=" + name + ", id=" + id + "]";
    }
}
