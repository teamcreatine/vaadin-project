package com.example.vaadinproject.windows;

import java.util.Set;

import com.example.vaadinproject.entities.ProductSupplier;
import com.example.vaadinproject.entities.Supplier;
import com.example.vaadinproject.layouts.ButtonSet;
import com.example.vaadinproject.layouts.DefaultVerticalLayout;
import com.example.vaadinproject.util.JPAHelper;
import com.vaadin.addon.jpacontainer.EntityItem;
import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.data.util.filter.Compare;
import com.vaadin.data.util.filter.Not;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ListSelect;
import com.vaadin.ui.Window;

@SuppressWarnings("unchecked")
/**
 * Old modal dialog to select suppliers. Should use the generic ListDialog class but this'll do for now.
 * 
 * @author Jukka Viljala <jupevi@utu.fi>
 *
 */
public class AddSuppliersDialog extends Window {

    private final ListSelect select;
    private final Button accept;

    /**
     * Creates a new modal dialog to select suppliers.
     * 
     * @deprecated use the generic ListDialog instead
     * @param excludedSuppliers
     *            suppliers to exclude from list
     */
    @Deprecated
    public AddSuppliersDialog(
            final JPAContainer<ProductSupplier> excludedSuppliers) {
        super("Select suppliers to add");

        DefaultVerticalLayout layout = new DefaultVerticalLayout();

        select = new ListSelect();
        select.setWidth(200, Unit.PIXELS);
        select.setNullSelectionAllowed(false);
        select.setItemCaptionPropertyId("name");
        select.setMultiSelect(true);

        final JPAContainer<?> suppliers = JPAHelper
                .getContainer(Supplier.class);

        boolean applyFilters = suppliers.isApplyFiltersImmediately();
        suppliers.setApplyFiltersImmediately(false);
        for (Object itemId : excludedSuppliers.getItemIds()) {
            EntityItem<ProductSupplier> item = excludedSuppliers
                    .getItem(itemId);
            suppliers.addContainerFilter(new Not(new Compare.Equal("id", item
                    .getEntity().getSupplier().getId())));
        }
        suppliers.applyFilters();
        suppliers.setApplyFiltersImmediately(applyFilters);
        select.setContainerDataSource(suppliers);

        accept = new Button("Accept");
        Button cancel = new Button("Cancel");
        ButtonSet buttons = new ButtonSet(accept, cancel);

        ClickListener closeListener = new ClickListener() {
            public void buttonClick(ClickEvent event) {
                AddSuppliersDialog.this.close();
            }
        };

        addCloseListener(new CloseListener() {
            @Override
            public void windowClose(CloseEvent e) {
                suppliers.removeAllContainerFilters();
            }
        });

        accept.addClickListener(closeListener);
        cancel.addClickListener(closeListener);

        layout.addComponents(select, buttons);

        setModal(true);
        setResizable(false);
        setContent(layout);
    }

    /**
     * Gets selected item ids.
     * 
     * @return selected item ids
     */
    public Set<Integer> getSelectedIds() {
        return (Set<Integer>) select.getValue();
    }

    public void addAcceptListener(ClickListener listener) {
        accept.addClickListener(listener);
    }

}
