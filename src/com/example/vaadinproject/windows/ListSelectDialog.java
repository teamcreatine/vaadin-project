package com.example.vaadinproject.windows;

import java.util.Set;

import com.example.vaadinproject.forms.fields.SimpleList;
import com.example.vaadinproject.layouts.ButtonSet;
import com.example.vaadinproject.layouts.DefaultVerticalLayout;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;

/**
 * Class to display a modal list select dialog where the user can select one or
 * many items from a list of entity items.
 * 
 * TODO: Allow excluding items from the list
 * 
 * @author Jukka Viljala <jupevi@utu.fi>
 * 
 * @param <T>
 *            type of entity to list
 */
public class ListSelectDialog<T> extends Window {

    DefaultVerticalLayout layout;
    SimpleList<T> list;
    Button acceptButton;
    Button cancelButton;
    ButtonSet buttons;

    /**
     * Creates a new modal list select dialog for an entity type.
     * 
     * @param type
     *            entity type to select
     * @param title
     *            title of the dialog window
     * @param columns
     *            columns used in entity listing
     */
    public ListSelectDialog(Class<T> type, String title, Object[] columns) {
        super(title);

        layout = new DefaultVerticalLayout();
        list = new SimpleList<T>(type, columns);
        acceptButton = new Button("Accept");
        cancelButton = new Button("Cancel");
        buttons = new ButtonSet(acceptButton, cancelButton);

        initLayout();
        initListeners();
    }

    /**
     * Initializes layout.
     */
    protected void initLayout() {
        setModal(true);
        setResizable(false);
        layout.addComponents(list, buttons);
        setContent(layout);
    }

    /**
     * Initializes listeners.
     */
    protected void initListeners() {
        // Support double clicking to accept
        list.addItemClickListener(new ItemClickListener() {
            public void itemClick(ItemClickEvent event) {
                if (event.isDoubleClick()) {
                    list.select(event.getItemId());
                    acceptButton.click();
                }
            }
        });

        ClickListener closeListener = new ClickListener() {
            public void buttonClick(ClickEvent event) {
                ListSelectDialog.this.close();
            }
        };

        acceptButton.addClickListener(closeListener);
        cancelButton.addClickListener(closeListener);
    }

    /**
     * Shows the dialog in the center of the UI.
     */
    public void show() {
        center();
        UI.getCurrent().addWindow(this);
    }

    /**
     * Gets selected entity from list.
     * 
     * @return selected entity
     */
    public T getSelectedEntity() {
        return list.getSelectedEntity();
    }

    /**
     * Gets set of selected entities from list.
     * 
     * @return set of selected entities
     */
    public Set<T> getSelectedEntities() {
        return list.getSelectedEntities();
    }

    public void addAcceptListener(ClickListener listener) {
        acceptButton.addClickListener(listener);
    }

    public void setMultiSelect(boolean enabled) {
        list.setMultiSelect(enabled);
    }

}
