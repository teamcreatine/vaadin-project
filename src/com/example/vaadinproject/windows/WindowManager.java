package com.example.vaadinproject.windows;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;

/**
 * Static class keeping track of singleton windows.
 * 
 * @author Jukka Viljala <jupevi@utu.fi>
 * 
 */
public class WindowManager {

    private static Map<String, Class<? extends CustomComponent>> pages;
    private static Map<String, Window> windows;

    public static Map<String, Class<? extends CustomComponent>> getPages() {
        if (pages == null) {
            pages = new LinkedHashMap<String, Class<? extends CustomComponent>>();
        }
        return pages;
    }

    public static Map<String, Window> getWindows() {
        if (windows == null) {
            windows = new HashMap<String, Window>();
        }
        return windows;
    }

    /**
     * Adds a new component to display in a window.
     * 
     * @param name
     *            name of component
     * @param page
     *            window content
     */
    public static void addPage(String name,
            Class<? extends CustomComponent> page) {
        getPages().put(name, page);
    }

    /**
     * Shows a window by key.
     * 
     * @param key
     *            window to show
     */
    public static void show(String key) {
        Window window = getWindows().get(key);
        if (window == null) {
            window = create(key);
        }
        if (window != null) {
            if (!UI.getCurrent().getWindows().contains(window)) {
                // If the user refreshed the page while this window was open, we
                // get an illegal argument exception. Detaching doesn't work, so
                // we have to re-create the window...
                if (window.isAttached()) {
                    window = create(key);
                }
                UI.getCurrent().addWindow(window);
            } else {
                window.bringToFront();
            }
        }
    }

    /**
     * Creates a new window instance for a key and caches it.
     * 
     * @param key
     *            window key
     * @return window instance
     */
    private static Window create(String key) {
        Window window = null;
        try {
            CustomComponent page = getPages().get(key).newInstance();
            window = new Window(key, page);
            window.center();
            getWindows().put(key, window);
        } catch (Exception e) {
            Notification
                    .show("Error getting page instance", Type.ERROR_MESSAGE);
            e.printStackTrace();
        }
        return window;
    }

    /**
     * Brings a window to front by key.
     * 
     * @param key
     *            window key
     */
    public static void bringToFront(String key) {
        Window window = getWindows().get(key);
        if (window != null) {
            window.bringToFront();
        }
    }

}
