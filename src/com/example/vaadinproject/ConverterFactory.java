package com.example.vaadinproject;

import java.util.Locale;

import com.example.vaadinproject.entities.BaseEntity;
import com.example.vaadinproject.entities.TimestampedEntity;
import com.example.vaadinproject.util.JPAHelper;
import com.vaadin.addon.jpacontainer.EntityItem;
import com.vaadin.data.util.converter.Converter;
import com.vaadin.data.util.converter.DefaultConverterFactory;

/**
 * Default converter factory for the application. Handles converting from
 * BaseEntity to id and back.
 * 
 * @author Jukka Viljala <jupevi@utu.fi>
 * 
 */
public class ConverterFactory extends DefaultConverterFactory {

    private BaseEntityConverter converter;

    @SuppressWarnings("unchecked")
    @Override
    protected <PRESENTATION, MODEL> Converter<PRESENTATION, MODEL> findConverter(
            Class<PRESENTATION> presentationType, Class<MODEL> modelType) {

        if (modelType.getSuperclass() == BaseEntity.class
                || modelType.getSuperclass() == TimestampedEntity.class) {
            if (converter == null) {
                converter = new BaseEntityConverter();
            }
            return (Converter<PRESENTATION, MODEL>) converter;
        }

        return super.findConverter(presentationType, modelType);
    }

    public class BaseEntityConverter implements Converter<Object, BaseEntity> {

        @Override
        public BaseEntity convertToModel(Object value,
                Class<? extends BaseEntity> targetType, Locale locale)
                throws com.vaadin.data.util.converter.Converter.ConversionException {
            if (targetType == BaseEntity.class) {
                return null;
            }
            EntityItem<?> item = JPAHelper.getItem(targetType, value);
            if (item != null) {
                return (BaseEntity) item.getEntity();
            }
            return null;
        }

        @Override
        public Object convertToPresentation(BaseEntity value,
                Class<? extends Object> targetType, Locale locale)
                throws com.vaadin.data.util.converter.Converter.ConversionException {
            if (value != null) {
                return value.getId();
            }
            return null;
        }

        @Override
        public Class<BaseEntity> getModelType() {
            return BaseEntity.class;
        }

        @Override
        public Class<Object> getPresentationType() {
            return Object.class;
        }

    }

}
